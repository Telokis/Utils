/**
*   @file   MetaParser.hh
*
*   @brief  Describes the @ref Utils::MetaParser class.
*/

#ifndef METAPARSER_HH_
#define METAPARSER_HH_

#include "meta_at_least_x_times.hh"
#include "meta_x_times.hh"
#include "meta_x_to_y_times.hh"
#include "meta_one_or_more.hh"
#include "meta_zero_or_more.hh"
#include "meta_zero_or_one.hh"
#include "interfaces/IProducterStream.hh"
#include "non_copyable.hh"
#include <stack>
#include <map>

namespace			Utils
{
  /**
  *   @class  MetaParser  MetaParser.hh <meta_parser.hh>
  *
  *   @brief  A base class for parsing.
  *
  *   MetaParser defines several useful methods to allow creation of
  *       advanced parsers.
  *   Its input is filled with an @ref IProducterStream.
  *
  *   @note A @ref  MetaParser can't be copied.
  *
  *   @see  @ref IProducterStream for input.
  */
  class				MetaParser : public non_copyable
  {
  public:
    /**
    *   @brief  Constructor
    *
    *   Initializes the internal IProducterStream to @a stream.
    *
    *   @param  stream      An IProducterStream used to read from.
    */
    MetaParser(IProducterStream &stream);

    /**
    *   @brief  Destructor
    */
    ~MetaParser();

  private:
    /**
    *   @brief  Updates the internal buffer.
    *
    *   Reads at least @a delta characters from @a _s.
    *   This function is lazy and is a noop if there already are @a delta
    *       character readable in the internal buffer.
    *   To force update look at MetaParser::forceUpdateBuffer.
    *
    *   @param  delta   The minimal number of characters that shoul be
    *                   accessible in internal buffer after call.
    *
    *   @return True if successful. False otherwise.
    */
    bool			updateBuffer(unsigned int delta = 1);

    /**
    *   @brief  Force the internal buffer to be updated
    *
    *   Updates the internal buffer whatever characters are still available.
    *
    *   @return True if successful. False otherwise.
    */
    bool			forceUpdateBuffer();

    /**
    *   @brief  Updates the cursor position.
    *
    *   The cursors moves by @a p characters.
    *
    *   @param  p The numbers of characters to move the
    *             cursor by. Defaults to 1.
    */
    void			incPos(int p = 1);

  public:
    /**
    *   @brief  Clears the internal buffer.
    *
    *   Releases characters read since last flush (or beginning).
    *
    *   @warning  Will return false if there are pending captures or
    *             if context' stack is not empty.
    *
    *   @return True if successful. False otherwise.
    *
    *   @see    MetaParser::validateContext and MetaParser::cancelCapture
    *           if it keeps returning false.
    */
    bool			flush();

  public:
    /**
    *   @name Context operations
    *   Context manipulations allow to parse more deeply.<br>
    *   It is possible to call different methods and to cancel everything
    *       if one of them fails.<br>
    *   Using those methods, an implementation of MetaParser::readNumber
    *       (a number matches [-+]?[0-9]+) could be :<br>
    *   @code
    *   bool		readNumber()
    *   {
    *     this->saveContext();                        // We save the context
    *     this->readChar('-') || this->readChar('+'); // We test if first char is [+-]
    *     if (this->readRange('0', '9'))              // If a figure is found
    *     {
    *       this->validateContext();                  // We validate the context : there is a number
    *       while (this->readRange('0', '9'));        // While there are figures we consume them.
    *       return (true);                            // We return true because it matched.
    *     }
    *     this->restoreContext();                     // If no figure is found, we restore the context
    *                                                 //    so that if [-+] was found, we cancel it.
    *     return (false);                             // Finally, we didn't match.
    *   }
    *   @endcode <br>
    */
    ///@{
    /**
    *   @brief  Pushes the current cursor position on stack.
    *
    *   Allows the parser to remember position.
    */
    void			saveContext();

    /**
    *   @brief  Moves the cursor to the last saved position.
    *
    *   Pops the last pushed position.
    *   Allows the parser to restore position.
    *
    *   @todo   Throw an exception if the stack is empty.
    */
    void			restoreContext();

    /**
    *   @brief  Pops the last pushed cursor position but do not move it.
    *
    *   Allows the parser to cancel the last save.
    */
    void			validateContext();
    ///@}

  public:
    /**
    *   @name Capture operations
    *   Captures allow to store read characters.<br>
    *   Here is an application example :<br>
    *   @code
    *   void		main()
    *   {
    *     MetaParser  p(ProducterString("-158 Hello !!"));      // Parser using a @ref ProducterString holding a string.
    *     std::string store;                                    // String used to store the result.
    *     std::string num;                                      // String used to store the number.
    *     
    *     p.beginCapture("all");                                // We start a new capture tagged "all".
    *     p.beginCapture("number");                             // We start a new capture tagged "number".
    *     if (p.readNumber() == false)                          // If there is no number to read.
    *       p.cancelCapture("number");                          // Then we cancel the capture tagged "number".
    *     else
    *       p.endCapture("number", num);                        // Otherwise, we store the number in num.
    *     p.readUntilEOF();                                     // Let's read everything.
    *     p.endCapture("all", store);                           // We store everything read in store.
    *     
    *     std::cout << "All: " << store << std::endl;           // Prints "All: -158 Hello !!"
    *     if (num.size())
    *       std::cout << "Num: \"" << num << "\"" << std::endl; // Prints "Num: -158"
    *     else
    *       std::cout << "No number read !" << std::endl;
    *   }
    *   @endcode <br>
    */
    ///@{
    /**
    *   @brief      Starts a new capture tagged @a tag.
    *
    *   @param[in]  tag The capture identifier.
    *
    *   @return     Always true.
    */
    bool			beginCapture(const std::string &tag);

    /**
    *   @brief      Ends a capture tagged @a tag and stores it in @a out.
    *
    *   @param[in]  tag The capture identifier.
    *   @param[out] out The string to store the result.
    *
    *   @return     True if successful. False if tag doesn't exist or
    *               if position is invalid (cursor moved backward).
    */
    bool			endCapture(const std::string &tag, std::string &out);

    /**
    *   @brief      Cancels capture tagged @a tag.
    *
    *   @param[in]  tag The capture identifier.
    *
    *   @return     Always true.
    */
    bool			cancelCapture(const std::string &tag);
    ///@}

  public:
    /**
    *   @brief      Compares the next character with @a c.
    *   @param[in]  c   The character to test.
    *   @return     True if @a c equals the next char. False otherwise.
    */
    bool			peekChar(char c);

    /**
    *   @brief      Compares the next character with @a c.
    *               Moves the cursor if a match is found.
    *   @param[in]  c   The character to test.
    *   @return     True if @a c equals the next char. False otherwise.
    */
    bool			readChar(char c);

    /**
    *   @brief      Tests if the next character if between @a begin and @a end.
    *               Moves the cursor if a match is found.
    *   @param[in]  begin   The character beginning the range.
    *   @param[in]  end     The character ending the range.
    *   @return     True if begin <= next character <= end. False otherwise.
    */
    bool			readRange(char begin, char end);

    /**
    *   @brief      Tests if the internal stream matches @a str.
    *               Moves the cursor by <em>str.size()</em> if a match is found.
    *   @param[in]  str   The string to find.
    *   @return     True if found. False otherwise.
    */
    bool			readText(const std::string &str);

    /**
    *   @brief      Tests if the internal stream matches @a str. Optimised for char*.
    *               Moves the cursor by <em>strlen(str)</em> if a match is found.
    *   @param[in]  str   The char* to find.
    *   @return     True if found. False otherwise.
    */
    bool			readText(const char *str);

    /**
    *   @brief      Tests if the next character is EOF.
    *   @return     True if the next character is EOF. False otherwise.
    */
    bool			readEOF();

    /**
    *   @brief      Reads while current char equals @a c.
    *               Moves the cursor by x if a match is found,
    *               x being the number of occurrences of @a c.
    *   @param[in]  c   The char to find.
    *   @return     True if found. False otherwise.
    */
    bool			readUntil(char c);

    /**
    *   @brief      Reads until the end.
    *               Moves the cursor to EOF.
    *   @return     True if EOF is found.
    *               False if it was previously found.
    */
    bool			readUntilEOF();

    /**
    *   @brief      Matches anything except EOF.
    *   @return     False if EOF is found. True otherwise.
    */
    bool			readAny();

  public:
    /**
    *   @brief      Matches an non-floating number.
    *               The model is [-+]?[0-9]+.
    *   @return     True if a number is found. False otherwise.
    */
    bool			readNumber();

    /**
    *   @brief      Matches an identifier.
    *               The model is [a-zA-Z_][a-zA-Z0-9_]*.
    *   @return     True if an identifier is found. False otherwise.
    */
    bool			readIdentifier();

  private:
    std::string			_buffer;
    IProducterStream		&_stream;
    size_t			_pos;
    std::stack<size_t>		_contexts;
    std::map<std::string, size_t>	_captures;
  };
}

#endif // METAPARSER_HH_
