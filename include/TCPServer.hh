#ifndef TCPSERVER_HH_
#define TCPSERVER_HH_

#include "interfaces/ISocketServer.hh"

#if defined(_WIN32) || defined(WIN32) // WINDOWS
#include "windows/TCPServerWindows.hh"

namespace Utils
{
	inline ISocketServer  *newTCPServer()
	{
		return (new Windows::TCPServerWindows);
	}
}

#elif defined(__unix) // UNIX
#include "unix/TCPServerUnix.hh"

namespace Utils
{
	inline ISocketServer  *newTCPServer()
	{
		return (new Unix::TCPServerUnix);
	}
}

#else
#error "Unknown platform"
#endif // Platform recognition

#endif // TCPSERVER_HH_
