/**
*   @file   static_parameter.hh
*
*   @brief  Contains definition of class Utils::static_parameter.
*/

#ifndef UTILS_STATIC_PARAMETER_HH_
#define UTILS_STATIC_PARAMETER_HH_

namespace Utils
{
  /**
  *	    @struct	static_parameter	static_parameter.hh	<small_object.hh>
  *
  *	    @tparam	T					The type that @ref static_parameter should represent.
  *	    @tparam	VALUE				The value of @a T that @ref static_parameter represents.
  *
  *	    @brief	Small object used to represent a static parameter.
  *
  *	    It represents a @a T value defined as a template parameter which can be a pointer.
  */
  template <typename T, T VALUE>
  struct static_parameter
  {
    typedef T type;					  /**< Typedef on @a T for easier reuse */
  };
}

#endif // UTILS_STATIC_PARAMETER_HH_