#ifndef IMUTEX_HH_
#define IMUTEX_HH_

#include <cstdlib>
#include <string>
#include "interfaces/ILockable.hh"
#include "Exceptions.hh"

namespace	Utils
{
  class		IMutex : public ILockable
  {
  public:
    virtual	~IMutex() {};
  }; // IMutex

  /**
   *	@namespace	Utils::Exceptions
   *	@brief		Namespace grouping all Exceptions used in Utils.
   */
  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(MutexLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(MutexRuntimeException, RuntimeException);
  }
} // Utils

#endif // IMUTEX_HH_
