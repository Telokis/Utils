#ifndef METHOD_WRAPPER_HH_
#define METHOD_WRAPPER_HH_

#include <helpers.hh>

#define HELL_GENERATE_METHOD_WRAPPER(x, mod)				\
  template <typename O,							\
	    typename R,							\
	    UTILS_GENERATE_X_TIMES(x, typename A)>			\
  struct	method_wrapper <O, R(O::*)(UTILS_GENERATE_X_TIMES(x, A)) mod> \
  {									\
    typedef R(O::*func_type)(UTILS_GENERATE_X_TIMES(x, A)) mod;		\
    method_wrapper(O *o, func_type f)					\
      : _o(o), _f(f)							\
    {									\
    }									\
    R		operator()(UTILS_GENERATE_DOUBLE_X_TIMES(x, A, a))	\
    {									\
      return (((*_o).*_f)(UTILS_GENERATE_X_TIMES(x, a)));		\
    }									\
  private:								\
    O		*_o;							\
    func_type	_f;							\
  };

template <typename O, typename F>
struct	method_wrapper;

template <typename O, typename R>
struct	method_wrapper <O, R(O::*)()>
{
  typedef R(O::*func_type)();
  method_wrapper(O *o, func_type f)
    : _o(o), _f(f)
  {
  }
  R		operator()()
  {
    return (((*_o).*_f)());
  }
private:
  O		*_o;
  func_type	_f;
};

HELL_GENERATE_METHOD_WRAPPER(1,);
HELL_GENERATE_METHOD_WRAPPER(2,);
HELL_GENERATE_METHOD_WRAPPER(3,);
HELL_GENERATE_METHOD_WRAPPER(4,);
HELL_GENERATE_METHOD_WRAPPER(5,);
HELL_GENERATE_METHOD_WRAPPER(6,);
HELL_GENERATE_METHOD_WRAPPER(7,);
HELL_GENERATE_METHOD_WRAPPER(8,);
HELL_GENERATE_METHOD_WRAPPER(9,);
HELL_GENERATE_METHOD_WRAPPER(10,);
HELL_GENERATE_METHOD_WRAPPER(11,);
HELL_GENERATE_METHOD_WRAPPER(12,);
HELL_GENERATE_METHOD_WRAPPER(13,);
HELL_GENERATE_METHOD_WRAPPER(14,);
HELL_GENERATE_METHOD_WRAPPER(15,);
HELL_GENERATE_METHOD_WRAPPER(16,);
HELL_GENERATE_METHOD_WRAPPER(17,);
HELL_GENERATE_METHOD_WRAPPER(18,);
HELL_GENERATE_METHOD_WRAPPER(19,);
HELL_GENERATE_METHOD_WRAPPER(20,);

// CONST FUNCTION
template <typename O, typename R>
struct	method_wrapper <O, R(O::*)() const>
{
  typedef R(O::*func_type)() const;
  method_wrapper(O *o, func_type f)
    : _o(o), _f(f)
  {
  }
  R		operator()()
  {
    return (((*_o).*_f)());
  }
private:
  O		*_o;
  func_type	_f;
};

HELL_GENERATE_METHOD_WRAPPER(1, const);
HELL_GENERATE_METHOD_WRAPPER(2, const);
HELL_GENERATE_METHOD_WRAPPER(3, const);
HELL_GENERATE_METHOD_WRAPPER(4, const);
HELL_GENERATE_METHOD_WRAPPER(5, const);
HELL_GENERATE_METHOD_WRAPPER(6, const);
HELL_GENERATE_METHOD_WRAPPER(7, const);
HELL_GENERATE_METHOD_WRAPPER(8, const);
HELL_GENERATE_METHOD_WRAPPER(9, const);
HELL_GENERATE_METHOD_WRAPPER(10, const);
HELL_GENERATE_METHOD_WRAPPER(11, const);
HELL_GENERATE_METHOD_WRAPPER(12, const);
HELL_GENERATE_METHOD_WRAPPER(13, const);
HELL_GENERATE_METHOD_WRAPPER(14, const);
HELL_GENERATE_METHOD_WRAPPER(15, const);
HELL_GENERATE_METHOD_WRAPPER(16, const);
HELL_GENERATE_METHOD_WRAPPER(17, const);
HELL_GENERATE_METHOD_WRAPPER(18, const);
HELL_GENERATE_METHOD_WRAPPER(19, const);
HELL_GENERATE_METHOD_WRAPPER(20, const);

// VOLATILE FUNCTION
template <typename O, typename R>
struct	method_wrapper <O, R(O::*)() volatile>
{
  typedef R(O::*func_type)() volatile;
  method_wrapper(O *o, func_type f)
    : _o(o), _f(f)
  {
  }
  R		operator()()
  {
    return (((*_o).*_f)());
  }
private:
  O		*_o;
  func_type	_f;
};

HELL_GENERATE_METHOD_WRAPPER(1, volatile);
HELL_GENERATE_METHOD_WRAPPER(2, volatile);
HELL_GENERATE_METHOD_WRAPPER(3, volatile);
HELL_GENERATE_METHOD_WRAPPER(4, volatile);
HELL_GENERATE_METHOD_WRAPPER(5, volatile);
HELL_GENERATE_METHOD_WRAPPER(6, volatile);
HELL_GENERATE_METHOD_WRAPPER(7, volatile);
HELL_GENERATE_METHOD_WRAPPER(8, volatile);
HELL_GENERATE_METHOD_WRAPPER(9, volatile);
HELL_GENERATE_METHOD_WRAPPER(10, volatile);
HELL_GENERATE_METHOD_WRAPPER(11, volatile);
HELL_GENERATE_METHOD_WRAPPER(12, volatile);
HELL_GENERATE_METHOD_WRAPPER(13, volatile);
HELL_GENERATE_METHOD_WRAPPER(14, volatile);
HELL_GENERATE_METHOD_WRAPPER(15, volatile);
HELL_GENERATE_METHOD_WRAPPER(16, volatile);
HELL_GENERATE_METHOD_WRAPPER(17, volatile);
HELL_GENERATE_METHOD_WRAPPER(18, volatile);
HELL_GENERATE_METHOD_WRAPPER(19, volatile);
HELL_GENERATE_METHOD_WRAPPER(20, volatile);

// VOLATILE CONST FUNCTION
template <typename O, typename R>
struct	method_wrapper <O, R(O::*)() const volatile>
{
  typedef R(O::*func_type)() const volatile;
  method_wrapper(O *o, func_type f)
    : _o(o), _f(f)
  {
  }
  R		operator()()
  {
    return (((*_o).*_f)());
  }
private:
  O		*_o;
  func_type	_f;
};

HELL_GENERATE_METHOD_WRAPPER(1, const volatile);
HELL_GENERATE_METHOD_WRAPPER(2, const volatile);
HELL_GENERATE_METHOD_WRAPPER(3, const volatile);
HELL_GENERATE_METHOD_WRAPPER(4, const volatile);
HELL_GENERATE_METHOD_WRAPPER(5, const volatile);
HELL_GENERATE_METHOD_WRAPPER(6, const volatile);
HELL_GENERATE_METHOD_WRAPPER(7, const volatile);
HELL_GENERATE_METHOD_WRAPPER(8, const volatile);
HELL_GENERATE_METHOD_WRAPPER(9, const volatile);
HELL_GENERATE_METHOD_WRAPPER(10, const volatile);
HELL_GENERATE_METHOD_WRAPPER(11, const volatile);
HELL_GENERATE_METHOD_WRAPPER(12, const volatile);
HELL_GENERATE_METHOD_WRAPPER(13, const volatile);
HELL_GENERATE_METHOD_WRAPPER(14, const volatile);
HELL_GENERATE_METHOD_WRAPPER(15, const volatile);
HELL_GENERATE_METHOD_WRAPPER(16, const volatile);
HELL_GENERATE_METHOD_WRAPPER(17, const volatile);
HELL_GENERATE_METHOD_WRAPPER(18, const volatile);
HELL_GENERATE_METHOD_WRAPPER(19, const volatile);
HELL_GENERATE_METHOD_WRAPPER(20, const volatile);

#endif // METHOD_WRAPPER_HH_
