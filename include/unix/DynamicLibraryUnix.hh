#ifndef DYNAMICLIBRARYUNIX_HH_
#define DYNAMICLIBRARYUNIX_HH_

#include "interfaces/IDynamicLibrary.hh"
#include "Exceptions.hh"
#include <string>
#include <dlfcn.h>

namespace		Utils
{
	/**
	 *	@namespace	Utils::Unix
	 *	@brief		Contains everything specific to Unix.
	 */
	namespace	Unix
	{
		class			DynamicLibraryUnix : public IDynamicLibrary
		{
		public:
			bool		load(std::string const &path);
			void		unload();
			void		*findFunc(std::string const &name);
			DynamicLibraryUnix();
			~DynamicLibraryUnix();

		private:
			void		*_lib;
		}; // DynamicLibraryUnix
	}
};

#endif // DYNAMICLIBRARYUNIX_HH_
