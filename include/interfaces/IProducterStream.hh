/**
*	@file   IProducterStream.hh
*
*	@brief  Defines an interface for all ProducterStreams to inherit
*       from.
*/

#ifndef IPRODUCTERSTREAM_HH_
#define IPRODUCTERSTREAM_HH_

#include <string>
#include "Exceptions.hh"

/**
*   @def      UTILS_PRODUCTER_STREAM_BUFFER_SIZE
*
*   @brief    Defines the buffer size for all ProducterStreams.
*
*   @ingroup  Macros
*/
#define UTILS_PRODUCTER_STREAM_BUFFER_SIZE	(4096)

namespace Utils
{
  /**
  *   @interface  IProducterStream  IProducterStream.hh <interfaces/IProducterStream.hh>
  *
  *   @brief      Describes the behavior expected for any ProducterStream.
  *
  *   A ProducterStream is a class which is responsible for handling a pseudo-stream.
  *   A pseudo-stream can be :<ul>
  *     <li>A std::string (@ref ProducterString)</li>
  *     <li>A file (@ref ProducterFile)</li>
  *     <li>std::cin (@ref ProducterCin)</li></ul>
  *
  *   The macro #UTILS_PRODUCTER_STREAM_BUFFER_SIZE is provided as an helper. It is not
  *       expected to be systematically used.
  *
  *   ProducterStreams are mainly used in combination with the @ref MetaParser.
  *
  *   @see        @ref ProducterString holds a std::string
  *   @see        @ref ProducterFile holds a file
  *   @see        @ref ProducterCin holds std::cin
  *   @see        #UTILS_PRODUCTER_STREAM_BUFFER_SIZE is an helper tool
  */
  class			IProducterStream
  {
  public:
    virtual		~IProducterStream() {}            /**< Virtual destructor. */

    /**
    *   @brief        Function used to get the next available chunk.
    *
    *   @param[out]   ret   A boolean which will be set to false if an error occurred or
    *                       if there were nothing left to read.
    *
    *   @return       The chunk to process.
    *
    *   @see          Example of implementation at @ref ProducterFile::nextString
    */
    virtual std::string	nextString(bool &ret) = 0;

    /**
    *   @brief        Function used to test whether the stream has been fully read or not.
    *
    *   @return       True if the stream is "finished". False otherwise.
    *
    *   @see          Example of implementation at @ref ProducterString::isEOF
    */
    virtual bool	isEOF() const = 0;
  };

  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(ProducterLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(ProducterRuntimeException, RuntimeException);
  }
}

#endif // IPRODUCTERSTREAM_HH_
