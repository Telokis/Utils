/**
*   @file   remove_pointer.hh
*   @brief  Describes Utils::remove_pointer.
*/

#ifndef UTILS_REMOVE_POINTER_HH_
#define UTILS_REMOVE_POINTER_HH_

#include "small_object/instance_of.hh"

namespace Utils
{
  /**
  *   @struct   remove_pointer  remove_pointer.hh   <type_traits.hh>
  *   @brief    removes a pointer qualifier from @a T.
  *
  *   remove_pointer<T *>::type == T.
  *
  *   Examples :
  *   @code
  *   remove_pointer<int>::type;              // int
  *   remove_pointer<int *>::type;            // int
  *   remove_pointer<int **>::type;           // int *
  *   remove_pointer<int const *>::type;      // int const
  *   remove_pointer<int * const>::type;      // int * const
  *   remove_pointer<int * &>::type;          // int * &
  *   remove_pointer<int(*)(int)>::type;      // int(int)     --> Function pointer changed to function.
  *   @endcode
  *
  *   @tparam   T The type to remove pointerness from.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  remove_pointer : instance_of<T>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for pointer T.
  */
  template <typename T>
  struct  remove_pointer <T *> : instance_of<T>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_REMOVE_POINTER_HH_