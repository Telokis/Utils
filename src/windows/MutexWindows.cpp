#include "windows/MutexWindows.hh"

Utils::Windows::MutexWindows::MutexWindows(LPSECURITY_ATTRIBUTES attr)
{
	InitializeCriticalSection(&this->_criticalSection);
}

Utils::Windows::MutexWindows::~MutexWindows()
{
	DeleteCriticalSection(&this->_criticalSection);
}

void	Utils::Windows::MutexWindows::lock()
{
	EnterCriticalSection(&this->_criticalSection);
}

void	Utils::Windows::MutexWindows::unlock()
{
	LeaveCriticalSection(&this->_criticalSection);
}

bool	Utils::Windows::MutexWindows::try_lock()
{
	if (TryEnterCriticalSection(&this->_criticalSection) != 0)
		return (true);
	return (false);
}