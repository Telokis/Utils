#include "meta_parser.hh"
#include <cstring>

// +------------+
// | DCstructor |
// +------------+
Utils::MetaParser::MetaParser(Utils::IProducterStream &stream)
  : _stream(stream), _pos(0)
{
  bool res;

  this->_buffer += this->_stream.nextString(res);
}

Utils::MetaParser::~MetaParser()
{
}

// +--------------------+
// | Internal functions |
// +--------------------+
bool		Utils::MetaParser::updateBuffer(unsigned int delta)
{
  bool		res;

  while (this->_buffer.size() - this->_pos < delta)
  {
    if (this->readEOF())
      return (false);
    this->_buffer += this->_stream.nextString(res);
    if (!res)
      return (false);
  }
  return (true);
}

bool		Utils::MetaParser::forceUpdateBuffer()
{
  bool		res;

  if (this->readEOF())
    return (false);
  this->_buffer += this->_stream.nextString(res);
  return (res);
}

void		Utils::MetaParser::incPos(int p)
{
  this->_pos += p;
}

// +-------+
// | Flush |
// +-------+
bool		Utils::MetaParser::flush()
{
  if (this->_contexts.empty() && this->_captures.empty())
  {
    this->_buffer.erase(0, this->_pos);
    this->_pos = 0;
    return (true);
  }
  return (false);
}

// +---------+
// | Context |
// +---------+
void		Utils::MetaParser::saveContext()
{
  this->_contexts.push(this->_pos);
}

void		Utils::MetaParser::restoreContext()
{
  this->_pos = this->_contexts.top();
  this->_contexts.pop();
}

void		Utils::MetaParser::validateContext()
{
  this->_contexts.pop();
}

// +----------+
// | Captures |
// +----------+
bool		Utils::MetaParser::beginCapture(const std::string &tag)
{
  this->_captures[tag] = this->_pos;
  return (true);
}

bool		Utils::MetaParser::endCapture(const std::string &tag, std::string &out)
{
  size_t	pos;

  if (this->_captures.find(tag) == this->_captures.end())
    return (false);
  pos = this->_captures.find(tag)->second;
  if (pos > this->_pos)
  {
    this->_captures.erase(tag);
    return (false);
  }
  out = this->_buffer.substr(pos, this->_pos - pos);
  this->_captures.erase(tag);
  return (true);
}

bool		Utils::MetaParser::cancelCapture(const std::string &tag)
{
  this->_captures.erase(tag);
  return (true);
}

// +-----------------+
// | Vital functions |
// +-----------------+
bool		Utils::MetaParser::peekChar(char c)
{
  if (this->updateBuffer() == false)
    return (false);
  return (this->_buffer.at(this->_pos) == c);
}

bool		Utils::MetaParser::readChar(char c)
{
  if (this->peekChar(c))
  {
    this->incPos();
    return (true);
  }
  return (false);
}

bool		Utils::MetaParser::readRange(char begin, char end)
{
  char		c;

  if (this->updateBuffer() == false)
    return (false);
  c = this->_buffer.at(this->_pos);
  if (c >= begin && c <= end)
  {
    this->incPos();
    return (true);
  }
  return (false);
}

bool		Utils::MetaParser::readText(const std::string &str)
{
  int		delta;

  delta = this->_buffer.size() - this->_pos;
  delta = str.size() - delta;
  if (delta > 0 && !this->_stream.isEOF())
  {
    if (this->updateBuffer(delta) == false)
      return (false);
  }
  if (this->_buffer.compare(this->_pos, str.size(), str) == 0)
  {
    this->incPos(str.size());
    return (true);
  }
  return (false);
}

bool		Utils::MetaParser::readText(const char *str)
{
  int		delta;
  size_t	len;

  len = strlen(str);
  delta = this->_buffer.size() - this->_pos;
  delta = len - delta;
  if (delta > 0 && !this->_stream.isEOF())
  {
    if (this->updateBuffer(delta) == false)
      return (false);
  }
  if (this->_buffer.compare(this->_pos, len, str) == 0)
  {
    this->incPos(len);
    return (true);
  }
  return (false);
}

bool		Utils::MetaParser::readEOF()
{
  if (this->_pos != this->_buffer.size())
    return (false);
  return (this->_stream.isEOF());
}

bool		Utils::MetaParser::readUntil(char c)
{
  size_t	r;

  r = this->_buffer.find(c, this->_pos);
  if (r == std::string::npos)
  {
    if (this->forceUpdateBuffer() == false)
      return (false);
    return (this->readUntil(c));
  }
  else if (r == std::string::npos)
    return (false);
  else
  {
    this->incPos(r - this->_pos);
    return (true);
  }
}

bool		Utils::MetaParser::readUntilEOF()
{
  this->_pos = this->_buffer.size();
  while (!this->_stream.isEOF())
  {
    if (this->updateBuffer() == false)
      return (false);
    this->_pos = this->_buffer.size();
  }
  return (true);
}

bool		Utils::MetaParser::readAny()
{
  if (this->updateBuffer() == false)
    return (false);
  this->incPos();
  return (true);
}

// +-----------------+
// | Bonus functions |
// +-----------------+
bool		Utils::MetaParser::readNumber()
{
  this->saveContext();
  this->readChar('-') || this->readChar('+');
  if (this->readRange('0', '9'))
  {
    this->validateContext();
    while (this->readRange('0', '9'));
    return (true);
  }
  this->restoreContext();
  return (false);
}

bool		Utils::MetaParser::readIdentifier()
{
  this->saveContext();
  if (this->readRange('a', 'z') ||
    this->readRange('A', 'Z') ||
    this->readChar('_'))
  {
    this->validateContext();
    while (this->readRange('a', 'z') ||
      this->readRange('A', 'Z') ||
      this->readChar('_') ||
      this->readRange('0', '9'));
    return (true);
  }
  this->restoreContext();
  return (false);
}
