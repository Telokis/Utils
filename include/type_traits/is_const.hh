/**
*   @file   is_const.hh
*   @brief  Describes Utils::is_const.
*/

#ifndef UTILS_IS_CONST_HH_
#define UTILS_IS_CONST_HH_

#include "small_object/selector.hh"

namespace Utils
{
  /**
  *   @struct   is_const  is_const.hh   <type_traits.hh>
  *   @brief    Tells whether @a T is const or not.
  *
  *   If @a T is const, is_const<T>::value == true.
  *
  *   @tparam   T The type to check for constness.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  is_const : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for const T.
  */
  template <typename T>
  struct  is_const <const T> : true_type
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_IS_CONST_HH_