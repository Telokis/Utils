#include "unix/DynamicLibraryUnix.hh"
#include <iostream>

Utils::Unix::DynamicLibraryUnix::DynamicLibraryUnix()
  : _lib(NULL)
{
}

Utils::Unix::DynamicLibraryUnix::~DynamicLibraryUnix()
{
  this->unload();
}

void	*Utils::Unix::DynamicLibraryUnix::findFunc(std::string const &name)
{
  if (!this->_lib)
	  throw Exceptions::DynamicLibraryLogicException("No library loaded");
  return (dlsym(this->_lib, name.c_str()));
}

bool	Utils::Unix::DynamicLibraryUnix::load(std::string const &path)
{
  this->unload();
  this->_lib = dlopen(path.c_str(), RTLD_LAZY);
  if (!this->_lib)
    return (false);
  return (true);
}

void	Utils::Unix::DynamicLibraryUnix::unload()
{
  int	ret;

  if (this->_lib != NULL)
  {
    ret = dlclose(this->_lib);
    if (ret != 0)
      throw Exceptions::DynamicLibraryRuntimeException("Error while unloading library");
  }
  this->_lib = NULL;
}