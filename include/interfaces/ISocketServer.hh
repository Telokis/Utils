#ifndef ISOCKETSERVER_HH_
#define ISOCKETSERVER_HH_

#include "ISocketClient.hh"
#include "IBasicSocketServer.hh"
#include "Exceptions.hh"

namespace			Utils
{
  class			       	ISocketServer : public IBasicSocketServer
  {
  public:
    virtual			~ISocketServer() {};
    virtual bool		start(unsigned short int port) = 0;
    virtual ISocketClient	*accept() const = 0;
  }; // ISocketServer

  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(TCPServerLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(TCPServerRuntimeException, RuntimeException);
  }

} // Utils

#endif // ISOCKETSERVER_HH_
