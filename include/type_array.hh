/**
*   @file   type_array.hh
*
*   @brief  Describes the type_array class.
*/

#ifndef UTILS_TYPE_ARRAY_HH_
#define UTILS_TYPE_ARRAY_HH_

#include "null_type.hh"
#include "type_list.hh"

namespace Utils
{
  /**
  *   @struct type_array  type_array.hh <type_array.hh>
  *
  *   @brief  Class containing up to 10 different types.
  *
  *   Less flexible alternative to @ref type_list.
  *   Internally holds a @ref type_list as @a type
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <
    typename T01 = null_type, typename T02 = null_type, typename T03 = null_type, typename T04 = null_type, typename T05 = null_type,
    typename T06 = null_type, typename T07 = null_type, typename T08 = null_type, typename T09 = null_type, typename T10 = null_type
  >
  struct  type_array
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      type_list <T06,
      type_list <T07,
      type_list <T08,
      type_list <T09,
      type_list <T10,
      null_type
      > > > > >
      > > > > >
      type;             /**< The associated Utils::type_list */
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   Partial specialization for 1 type.
  *   @see  @ref Utils::type_array
  */
  template<typename T01>
  struct type_array < T01 >
  {
    typedef
      type_list < T01,
      null_type
      >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 2 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02>
  struct type_array < T01, T02 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      null_type
      > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 3 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03>
  struct type_array < T01, T02, T03 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      null_type
      > > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 4 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04>
  struct type_array < T01, T02, T03, T04 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      null_type
      > > > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 5 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04, typename T05>
  struct type_array < T01, T02, T03, T04, T05 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      null_type
      > > > > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 6 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04, typename T05,
           typename T06>
  struct type_array < T01, T02, T03, T04, T05,
                      T06 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      type_list <T06,
      null_type
      > > > > >
      >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 7 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04, typename T05,
           typename T06, typename T07>
  struct type_array < T01, T02, T03, T04, T05,
                      T06, T07 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      type_list <T06,
      type_list <T07,
      null_type
      > > > > >
      > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 8 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04, typename T05,
           typename T06, typename T07, typename T08>
  struct type_array < T01, T02, T03, T04, T05,
                      T06, T07, T08 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      type_list <T06,
      type_list <T07,
      type_list <T08,
      null_type
      > > > > >
      > > >
      type;             /**< The associated Utils::type_list */
  };

  /**
  *   Partial specialization for 9 types.
  *   @see  @ref Utils::type_array
  */
  template<typename T01, typename T02, typename T03, typename T04, typename T05,
           typename T06, typename T07, typename T08, typename T09>
  struct type_array < T01, T02, T03, T04, T05,
                      T06, T07, T08, T09 >
  {
    typedef
      type_list <T01,
      type_list <T02,
      type_list <T03,
      type_list <T04,
      type_list <T05,
      type_list <T06,
      type_list <T07,
      type_list <T08,
      type_list <T09,
      null_type
      > > > > >
      > > > >
      type;             /**< The associated Utils::type_list */
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_TYPE_ARRAY_HH_