/**
*   @file   static_value.hh
*
*   @brief  Contains definition of class Utils::static_value.
*/

#ifndef UTILS_STATIC_VALUE_HH_
#define UTILS_STATIC_VALUE_HH_

#include "small_object/static_parameter.hh"

namespace Utils
{
  /**
  *	    @struct	static_value	static_value.hh	<small_object.hh>
  *
  *	    @tparam	T				The type that @ref static_value should represent.
  *	    @tparam	VALUE			The value of @a T that @ref static_value represents.
  *
  *	    @brief	Small object used to represent a static value.
  *
  *   	It represents a @a T value defined as a template parameter.
  *   	It defines a @ref value member which must be of integer type.
  * 	For declaring pointer statically, use @ref static_parameter instead.
  */
  template <typename T, T VALUE>
  struct static_value : static_parameter < T, VALUE >
  {
    static const T value = VALUE;	  /**< The value @a VALUE to ease access */
  };
}

#endif // UTILS_STATIC_VALUE_HH_