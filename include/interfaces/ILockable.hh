#ifndef ILOCKABLE_HH_
#define ILOCKABLE_HH_

#include "non_copyable.hh"

namespace			Utils
{

  class				ISimpleLockable : public non_copyable
  {
  public:
    virtual			~ISimpleLockable() {}
    virtual void	lock() = 0;
    virtual void	unlock() = 0;
  }; // ISimpleLockable

  class				ILockable : public ISimpleLockable
  {
  public:
    virtual			~ILockable() {}
    virtual bool	try_lock() = 0;
  }; // ILockable

} // Utils

#endif // ILOCKABLE_HH_
