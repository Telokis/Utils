#ifndef META_AT_LEAST_X_TIMES_HH_
#define META_AT_LEAST_X_TIMES_HH_

namespace	Utils
{

  //////////////////////////////////////////
  //                                      //
  // -------------- WARNING ------------- //
  // Keep track of the context !!!        //
  //                                      //
  //////////////////////////////////////////

  // +-------------------------+
  // | For a pointer to method |
  // +-------------------------+

  // For zero parameter
  template <class P>
  inline bool	at_least_x_times(unsigned int n, bool (P::*func)(), P &parser)
  {
    unsigned int	i = 0;

    while ((parser.*func)())
      i++;
    return (i >= n);
  }

  // For one parameter
  template <class P, class A0>
  inline bool	at_least_x_times(unsigned int n, bool (P::*func)(A0), P &parser, A0 arg0)
  {
    unsigned int	i = 0;

    while ((parser.*func)(arg0))
      i++;
    return (i >= n);
  }

  // For two parameters
  template <class P, class A0, class A1>
  inline bool	at_least_x_times(unsigned int n, bool (P::*func)(A0, A1), P &parser, A0 arg0, A1 arg1)
  {
    unsigned int	i = 0;

    while ((parser.*func)(arg0, arg1))
      i++;
    return (i >= n);
  }

  // +---------------------------+
  // | For a pointer to function |
  // +---------------------------+

  // For zero parameter
  inline bool	at_least_x_times(unsigned int n, bool (*func)())
  {
    unsigned int	i = 0;

    while (func())
      i++;
    return (i >= n);
  }

  // For one parameter
  template <class A0>
  inline bool	at_least_x_times(unsigned int n, bool (*func)(A0), A0 arg0)
  {
    unsigned int	i = 0;

    while (func(arg0))
      i++;
    return (i >= n);
  }

  // For two parameters
  template <class A0, class A1>
  inline bool	at_least_x_times(unsigned int n, bool (*func)(A0, A1), A0 arg0, A1 arg1)
  {
    unsigned int	i = 0;

    while (func(arg0, arg1))
      i++;
    return (i >= n);
  }

}

#endif // META_AT_LEAST_X_TIMES_HH_
