/**
*   @file   add_pointer.hh
*   @brief  Describes Utils::add_pointer.
*/

#ifndef UTILS_ADD_POINTER_HH_
#define UTILS_ADD_POINTER_HH_

#include "small_object/instance_of.hh"

namespace Utils
{
  /**
  *   @struct   add_pointer  add_pointer.hh   <type_traits.hh>
  *   @brief    Adds a pointer qualifier to @a T.
  *
  *   add_pointer<T>::type == T*.
  *
  *   Examples :
  *   @code
  *   add_pointer<int>::type;           // int *
  *   add_pointer<int const&>::type;    // int const*
  *   add_pointer<int *>::type;         // int **
  *   add_pointer<int *&>::type;        // int **
  *   add_pointer<int(int)>::type;      // int (*)(int)
  *   @endcode
  *
  *   @tparam   T The type to add pointerness on.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  add_pointer : instance_of<T*>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for T&.
  */
  template <typename T>
  struct  add_pointer <T&> : instance_of<T*>
  {
  };

  /**
  *   @brief    Partial specialization for void. Error will occur.
  */
  template <>
  struct  add_pointer <void>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_ADD_POINTER_HH_