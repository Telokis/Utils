#ifndef TCPSERVERWINDOWS_HH_
#define TCPSERVERWINDOWS_HH_

#include "interfaces/ISocketServer.hh"
#include "Exceptions.hh"
#include <Winsock2.h>

namespace				Utils
{
	namespace Windows
	{
		class					TCPServerWindows : public ISocketServer
		{
		public:
			TCPServerWindows();
			~TCPServerWindows();

		public:
			virtual bool		start(unsigned short int port);
			virtual ISocketClient	*accept() const;

		private:
			SOCKET				_socket;
		}; // TCPServerWindows
	}
} // Utils

#endif // TCPSERVERWINDOWS_HH_
