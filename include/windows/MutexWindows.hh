#ifndef MUTEXWINDOWS_HH_
#define MUTEXWINDOWS_HH_

#include "interfaces/IMutex.hh"
#include <string>
#include "Exceptions.hh"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace				Utils
{
	namespace Windows
	{
		class					MutexWindows : public IMutex
		{
		public:
			MutexWindows(LPSECURITY_ATTRIBUTES attr = NULL);
			~MutexWindows();
			virtual void		lock();
			virtual void		unlock();
			virtual bool		try_lock();

		private:
			CRITICAL_SECTION	_criticalSection;
		}; // MutexWindows
	}
} // Utils

#endif // MUTEXWINDOWS_HH_
