/**
*   @file   is_reference.hh
*   @brief  Describes Utils::is_reference.
*/

#ifndef UTILS_IS_REFERENCE_HH_
#define UTILS_IS_REFERENCE_HH_

#include <small_object/selector.hh>

namespace Utils
{
  /**
  *   @struct   is_reference  is_reference.hh   <type_traits.hh>
  *   @brief    Tells whether @a T is reference or not.
  *
  *   If @a T is reference, is_reference<T>::value == true.
  *
  *   @tparam   T The type to check for reference.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  is_reference : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for T&.
  */
  template <typename T>
  struct  is_reference <T &> : true_type
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_IS_REFERENCE_HH_
