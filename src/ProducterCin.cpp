#include "ProducterCin.hh"

Utils::ProducterCin::ProducterCin()
{
}

Utils::ProducterCin::~ProducterCin()
{
}

std::string	Utils::ProducterCin::nextString(bool &ret)
{
  char		buffer[UTILS_PRODUCTER_STREAM_BUFFER_SIZE] = { 0 };

  ret = true;
  std::cin.getline(buffer, UTILS_PRODUCTER_STREAM_BUFFER_SIZE);
  if (std::cin.gcount() < 1 || std::cin.fail())
  {
    ret = false;
    return (std::string(""));
  }
  return (std::string(buffer));
}

bool		Utils::ProducterCin::isEOF() const
{
  return (std::cin.eof());
}
