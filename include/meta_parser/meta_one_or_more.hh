#ifndef META_ONE_OR_MORE_HH_
#define META_ONE_OR_MORE_HH_

namespace Utils
{

  // +-------------------------+
  // | For a pointer to method |
  // +-------------------------+

  // For zero parameter
  template <class P>
  inline bool	one_or_more(bool (P::*func)(), P &parser)
  {
    if ((parser.*func)())
      {
	while ((parser.*func)());
	return (true);
      }
    return (false);
  }

  // For one parameter
  template <class P, class A0>
  inline bool	one_or_more(bool (P::*func)(A0), P &parser, A0 arg0)
  {
    if ((parser.*func)(arg0))
      {
	while ((parser.*func)(arg0));
	return (true);
      }
    return (false);
  }

  // For two parameters
  template <class P, class A0, class A1>
  inline bool	one_or_more(bool (P::*func)(A0, A1), P &parser, A0 arg0, A1 arg1)
  {
    if ((parser.*func)(arg0, arg1))
      {
	while ((parser.*func)(arg0, arg1));
	return (true);
      }
    return (false);
  }

  // +---------------------------+
  // | For a pointer to function |
  // +---------------------------+

  // For zero parameter
  inline bool	one_or_more(bool (*func)())
  {
    if (func())
      {
	while (func());
	return (true);
      }
    return (false);
  }

  // For one parameter
  template <class A0>
  inline bool	one_or_more(bool (*func)(A0), A0 arg0)
  {
    if (func(arg0))
      {
	while (func(arg0));
	return (true);
      }
    return (false);
  }

  // For two parameters
  template <class A0, class A1>
  inline bool	one_or_more(bool (*func)(A0, A1), A0 arg0, A1 arg1)
  {
    if (func(arg0, arg1))
      {
	while (func(arg0, arg1));
	return (true);
      }
    return (false);
  }

}

#endif // META_ONE_OR_MORE_HH_
