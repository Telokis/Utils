#ifndef WEAKPTR_HH_
#define WEAKPTR_HH_

#include "SharedPtr.hh"

namespace       Utils
{

  template <typename T>
  class         WeakPtr
  {
  public:
    WeakPtr(const SharedPtr<T> &shared)
      : _ptr(shared.get())
    {
    }

    WeakPtr()
      : _ptr(NULL)
    {
    }

    ~WeakPtr()
    {
    }

    WeakPtr     &operator=(const SharedPtr<T> &ptr)
    {
      this->_ptr = ptr.get();
      return (*this);
    }

    T           &operator*() const
    {
      return (*this->_ptr);
    }

    template <typename U>
    bool        operator==(SharedPtr<U> ptr) const
    {
      return (this->_ptr == ptr.get());
    }

    template <typename U>
    bool        operator==(WeakPtr<U> ptr) const
    {
      return (this->_ptr == ptr.get());
    }

    T           *operator->() const
    {
      return (this->_ptr);
    }

    T           *get() const
    {
      return (this->_ptr);
    }

    bool        operator==(void *ptr) const
    {
      return (this->_ptr == ptr);
    }

  private:
    T           *_ptr;
    WeakPtr(const WeakPtr<T> &_ref);
    WeakPtr     &operator=(const WeakPtr<T> &_ref);
  }; // WeakPtr

} // Utils

#endif // WEAKPTR_HH_
