/**
 *	@file	SharedPtr.hh
 *	@brief	Describes the SharedPtr class.
 */

#ifndef SHAREDPTR_HH_
#define SHAREDPTR_HH_

#include <cstdlib>
#include <iostream>
#include "Mutex.hh"

namespace	Utils
{
  /**
  *	@class	SharedPtr	SharedPtr.hh	<SharedPtr.hh>
  *	@brief	Smart pointer holding a <em>T*</em>.
  *
  *	Represents a shared resource.
  *	Ownership is shared between multiple @ref SharedPtr instances.
  *	The last instance being deleted is responsible for releasing memory
  *		hold.
  *	SharedPtr is designed to be used as transparently as a nude-pointer.
  *	To do so, it defines a lot of common operators.
  *
  *	@tparam	T	Type to hold. <em>T*</em> is internally stored.
  *
  *	@note	Any operation on @ref SharedPtr is thread safe.
  *
  *	@see	@ref unique_ptr	if ownership should not be shared.
  */
  template <typename T>
  class		SharedPtr
  {
  public:
    typedef	T	ptr_type;										/**< Typedef on @a T for easier usage. */
    typedef	void(*delete_func)(T *);							/**< Typedef on @a T's expected destructor. */

    /**
     *	@brief	Default constructor.
     *
     *	Initializes the internal pointer to @a ptr and set the deleter to @a f.
     *	Defaults to NULL if no parameters are passed.
     *
     *	@param	ptr	The pointer to hold.
     *	@param	f	The deleter to use.
     */
    SharedPtr(ptr_type *ptr = NULL, delete_func f = NULL)
      : _ptr(ptr), _d(f)
    {
      this->_count = new _Count(ptr);
      ScopedMutex(this->_count->_mutex);
      if (this->_count->_count == 0)
        this->_count->_count = 1;
      else
        this->_count->_count += 1;
    }

    /**
     *	@brief	Copy constructor.
     *
     *	Initializes the internal pointer to @a model's internal pointer and set the deleter to @a model's.
     *	Increases the references count by one.
     *
     *	@param	model	The @ref SharedPtr to copy.
     */
    SharedPtr(const SharedPtr<T> &model)
      : _ptr(model.get()), _d(model._d)
    {
      this->_count = model._count;
      if (this->_count)
      {
        ScopedMutex(this->_count->_mutex);
        if (this->_count->_count == 0)
          this->_count->_count = 1;
        else
          this->_count->_count += 1;
      }
    }

    /**
     *	@brief	Destructor.
     *
     *	Destroys the current @ref SharedPtr instance.
     *	Decreases the references count by one.
     *	If the references count becomes 0, the internal pointer is deleted.
     *	Uses @a delete_func if provided and delete operator otherwise.
     */
    ~SharedPtr()
    {
      if (this->_count)
      {
        ScopedMutex(this->_count->_mutex);
        if (this->_count->_count == 1)
        {
          if (this->_d == NULL)
            delete (this->_ptr);
          else
            this->_d(this->_ptr);
        }
        this->_count->_count -= 1;
        if (this->_count->_count == 0)
        {
          delete (this->_count);
          this->_count = NULL;
        }
      }
    }

    /**
     *	@brief  Overloads the << operator for a std::ostream.
     *
     *	Allows the SharedPtr to be written on a stream as if the
     *		internal pointer was.
     *
     *  @return The stream passed as parameter.
     */
    std::ostream  &operator<<(const std::ostream &os)
    {
      os << this->_ptr;
      return (os);
    }

    /**
     *	@brief	Conversion operator allowing SharedPtr<T> to be treated as a
     *		simple T*.
     *
     *  @return The internal pointer.
     */
    operator T*()
    {
      return (this->get());
    }

    /**
     *	@brief	Addressof operator.
     *
     *  @return The address of the internal pointer.
     */
    T			  **operator&()
    {
      return (&this->_ptr);
    }

    /**
     *	@brief	Assignment operator from another @ref SharedPtr.
     *
     *	Initializes the internal pointer to @a model's internal pointer and set the deleter to @a model's.
     *	Increases the references count of model by one.
     *	By the same time, it decreases its own internal count by one and
     *		releases memory if necessary.
     *
     *	@param	model	The @ref SharedPtr to copy.
     *
     *  @return The current @ref SharedPtr instance to allow chaining operations.
     */
    SharedPtr	  &operator=(const SharedPtr &model)
    {
      _Count	  *tmp;
      ptr_type	  *ptr;

      if (this->_count)
        ScopedMutex(this->_count->_mutex);
      if (this->_count != model._count && model._count)
        ScopedMutex(model._count->_mutex);
      ptr = model.get();
      tmp = model._count;
      if (tmp)
      {
        if (tmp->_count == 0)
          tmp->_count = 1;
        else
          tmp->_count += 1;
      }
      if (this->_count)
      {
        if (this->_count->_count == 1)
        {
          if (this->_d == NULL)
            delete (this->_ptr);
          else
            this->_d(this->_ptr);
        }
        this->_count->_count -= 1;
        if (this->_count->_count == 0)
          delete (this->_count);
      }
      this->_count = tmp;
      this->_ptr = ptr;
      this->_d = model._d;
      return (*this);
    }

    /**
     *	@brief	Registers a new deleter.
     *
     *	@param	f	The @ref delete_func to register.
     */
    void	      registerDestroyFunc(delete_func f)
    {
      this->_d = f;
    }

    /**
     *	@brief	Assignment operator from a nude pointer of type @a ptr_type.
     *
     *	Initializes the internal pointer to @a ptr.
     *	Increases the references count of @a ptr by one.
     *	By the same time, it decreases its own internal count by one and
     *		releases memory if necessary.
     *
     *	@param	ptr	The nude ptr to hold.
     *
     *  @return The current @ref SharedPtr instance to allow chaining operations.
     */
    SharedPtr	  &operator=(ptr_type *ptr)
    {
      _Count	  *tmp;

      tmp = new _Count(ptr);
      if (this->_count)
        ScopedMutex(this->_count->_mutex);
      if (tmp->_count == 0)
        tmp->_count = 1;
      else
        tmp->_count += 1;
      if (this->_count)
      {
        if (this->_count->_count == 1)
        {
          if (this->_d == NULL)
            delete (this->_ptr);
          else
            this->_d(this->_ptr);
        }
        this->_count->_count -= 1;
        if (this->_count->_count == 0)
          delete (this->_count);
      }
      this->_count = tmp;
      this->_ptr = ptr;
      return (*this);
    }

    /**
     *	@brief	Indirection operator.
     *
     *  @return The ptr to indirect.
     */
    ptr_type	  &operator*() const
    {
      return (*this->_ptr);
    }

    /**
     *	@brief	Equality operator taking a nude pointer.
     *
     *	@param	ptr	The comparing nude pointer.
     *
     *  @return True if the internal ptr is equal to @a ptr. False otherwise.
     */
    bool	      operator==(void *ptr) const
    {
      return (this->_ptr == ptr);
    }

    /**
     *	@brief	Equality operator taking a @ref SharedPtr templated on a different type.
     *
     *	@param	ptr	The comparing @ref SharedPtr.
     *
     *  @return True if the internal ptr is equal to @a ptr's one. False otherwise.
     */
    template <typename U>
    bool	      operator==(const SharedPtr<U> &ptr) const
    {
      return (this->_ptr == ptr.get());
    }

    /**
     *	@brief	NotEqual operator taking a nude pointer.
     *
     *	@param	ptr	The comparing nude pointer.
     *
     *  @return True if the internal ptr is different from @a ptr. False otherwise.
     */
    bool	      operator!=(void *ptr) const
    {
      return (this->_ptr != ptr);
    }

    /**
     *	@brief	NotEqual operator taking a @ref SharedPtr templated on a different type.
     *
     *	@param	ptr	The comparing @ref SharedPtr.
     *
     *  @return True if the internal ptr is different from @a ptr's one. False otherwise.
     */
    template <typename U>
    bool	      operator!=(const SharedPtr<U> &ptr) const
    {
      return (this->_ptr != ptr.get());
    }

    /**
     *	@brief	Member of pointer operator.
     *
     *  @return The internal pointer to access.
     */
    ptr_type	  *operator->() const
    {
      return (this->_ptr);
    }

    /**
     *	@brief	Resets a SharedPtr to NULL.
     *
     *	Decrements the reference count and releases memory hold by internal
     *		pointer if needed.
     *	Comparison with NULL is now true.
     */
    void	      reset()
    {
      if (this->_count)
      {
        ScopedMutex(this->_count->_mutex);
        if (this->_count->_count == 1)
        {
          if (this->_d == NULL)
            delete (this->_ptr);
          else
            this->_d(this->_ptr);
        }
        this->_count->_count -= 1;
        if (this->_count->_count == 0)
          delete (this->_count);
      }
      this->_count = NULL;
      this->_ptr = NULL;
    }

    /**
     *	@brief	Nude pointer accessor.
     *
     *  @return The internal pointer.
     */
    ptr_type	  *get() const
    {
      return (this->_ptr);
    }

    /**
     *	@brief	Deleter accessor.
     *
     *  @return The deleter function. NULL if not set.
     */
    delete_func	  getDeletor() const
    {
      return (this->_d);
    }

  private:
    class	_Count
    {
    public:
      ~_Count()
      {
        delete (this->_mutex);
      }
      _Count(T *ptr)
        : _ptr(ptr), _mutex(newMutex()), _count(0)
      {}
      T		  *_ptr;
      IMutex  *_mutex;
      int	  _count;
    }; // _Count

  private:
    _Count	      *_count;
    ptr_type	  *_ptr;
    delete_func	  _d;
    static IMutex *_mutex;
  }; // SharedPtr

  template <typename T> IMutex	*SharedPtr<T>::_mutex = newMutex();
};

#endif // SHAREDPTR_HH_
