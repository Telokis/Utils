#ifndef DYNAMICLIBRARY_HH_
#define DYNAMICLIBRARY_HH_

#include "interfaces/IDynamicLibrary.hh"

#if defined(_WIN32) || defined(WIN32) // WINDOWS
#include "windows/DynamicLibraryWindows.hh"

namespace Utils
{
	inline IDynamicLibrary	*newDynamicLibrary()
	{
		return (new Windows::DynamicLibraryWindows);
	}
}

#elif defined(__unix) // UNIX
#include "unix/DynamicLibraryUnix.hh"

namespace Utils
{
	inline IDynamicLibrary	*newDynamicLibrary()
	{
		return (new Unix::DynamicLibraryUnix);
	}
}
#else
#error "Unknown platform"
#endif // Platform recognition

#endif // DYNAMICLIBRARY_HH_
