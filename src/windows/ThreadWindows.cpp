#include "windows/ThreadWindows.hh"
#include "SharedPtr.hh"
#include <iostream>

Utils::Windows::ThreadWindows::ThreadWindows()
  : _started(false)
{
}

Utils::Windows::ThreadWindows::ThreadWindows(IThreadFunctor *functor, void *arg)
  : _started(false)
{
  this->launch(functor, arg);
}

Utils::Windows::ThreadWindows::ThreadWindows(void(*func)(void *), void *arg)
  : _started(false)
{
  this->launch(func, arg);
}

Utils::Windows::ThreadWindows::~ThreadWindows()
{
}

Utils::Windows::ThreadWindows::ThreadBasicLauncher::ThreadBasicLauncher(void(*f)(void *))
  : _f(f)
{
}

Utils::Windows::ThreadWindows::ThreadBasicLauncher::~ThreadBasicLauncher()
{
}

void	Utils::Windows::ThreadWindows::ThreadBasicLauncher::operator()(void *arg)
{
  _f(arg);
}

Utils::Windows::ThreadWindows::ThreadBindLauncher::ThreadBindLauncher(const Utils::base_functor<void> &f)
  : _f(f)
{
}

Utils::Windows::ThreadWindows::ThreadBindLauncher::~ThreadBindLauncher()
{
}

void	Utils::Windows::ThreadWindows::ThreadBindLauncher::operator()(void *)
{
  _f();
}

DWORD WINAPI		Utils::Windows::ThreadWindows::_handler(LPVOID arg)
{
  FunctorParams	    *params = static_cast<FunctorParams *>(arg);

  (*params->func)(params->param);
  return (0);
}

void				Utils::Windows::ThreadWindows::launch(IThreadFunctor *functor, void *arg)
{
  FunctorParams	    *params = new FunctorParams(functor, arg);

  if (_started == true)
	  throw Exceptions::ThreadLogicException("Thread already launched");
  if ((this->_thread = CreateThread(NULL, 0, _handler, params, 0, NULL)) == NULL)
	  throw Exceptions::ThreadRuntimeException("Could not initialize thread");
  _started = true;
}

void					Utils::Windows::ThreadWindows::launch(void(*func)(void *), void *arg)
{
  ThreadBasicLauncher	*_threadBasicLauncher = new ThreadBasicLauncher(func);

  this->launch(_threadBasicLauncher, arg);
}

void			Utils::Windows::ThreadWindows::launch(const base_functor<void> &f)
{
  ThreadBindLauncher	*_threadBindLauncher = new ThreadBindLauncher(f);

  this->launch(_threadBindLauncher);
}

void		Utils::Windows::ThreadWindows::join()
{
  if (_started == false)
	  throw Exceptions::ThreadLogicException("Thread wasn't running");
  if (WaitForSingleObject(this->_thread, INFINITE) != WAIT_OBJECT_0)
	  throw Exceptions::ThreadLogicException("Could not join thread");
}

void            Utils::Windows::ThreadWindows::terminate()
{
  TerminateThread(_thread, 0);
}