/**
 *   @file   is_function.hh
 *   @brief  Describes Utils::is_function.
 */

#ifndef UTILS_IS_FUNCTION_HH_
#define UTILS_IS_FUNCTION_HH_

#include <small_object/selector.hh>
#include <helpers.hh>

#define UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(x)		\
  template <typename R, UTILS_GENERATE_X_TIMES(x, typename A)>	\
  struct  is_function < R(UTILS_GENERATE_X_TIMES(x, A)) >	\
    : true_type {};

namespace Utils
{
  /**
   *   @struct   is_function  is_function.hh   <type_traits.hh>
   *   @brief    Tells whether @a T is function or not.
   *
   *   If @a T is function, is_function<T>::value == true.
   *
   *   @tparam   T The type to check for functionness.
   *
   *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
   *             show specialized versions.
   */
  template <typename T>
  struct  is_function : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
   *   @brief    Partial specialization for functions.
   */
  template <typename R>
  struct  is_function < R() > : true_type {};

  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(1);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(2);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(3);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(4);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(5);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(6);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(7);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(8);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(9);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(10);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(11);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(12);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(13);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(14);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(15);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(16);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(17);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(18);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(19);
  UTILS_PRIVATE_GENERATE_IS_FUNCTION_UP_TO(20);

#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_IS_FUNCTION_HH_
