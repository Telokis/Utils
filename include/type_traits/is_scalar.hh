/**
 *   @file   is_scalar.hh
 *   @brief  Describes Utils::is_scalar.
 */

#ifndef UTILS_IS_SCALAR_HH_
#define UTILS_IS_SCALAR_HH_

#include <small_object/selector.hh>

#define UTILS_PRIVATE_GENERATE_IS_SCALAR(T)	\
  template <>					\
  struct  is_scalar <T> : true_type {};

namespace Utils
{
  /**
   *   @struct   is_scalar  is_scalar.hh   <type_traits.hh>
   *   @brief    Tells whether @a T is scalar or not.
   *
   *   If @a T is scalar, is_scalar<T>::value == true.
   *
   *   @tparam   T The type to check for scalarness.
   *
   *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
   *             show specialized versions.
   */
  template <typename T>
  struct  is_scalar : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
   *   @brief    Partial specialization for T*.
   */
  template <typename T>
  struct  is_scalar <T *> : true_type
  {
  };

  /**
   *   @brief    Partial specialization for scalars.
   */
  UTILS_PRIVATE_GENERATE_IS_SCALAR(char);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(unsigned char);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(short);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(unsigned short);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(int);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(unsigned int);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(long);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(unsigned long);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(float);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(bool);
  UTILS_PRIVATE_GENERATE_IS_SCALAR(double);

  /**
   *   @brief    Partial specialization for const T.
   */
  template <typename T>
  struct  is_scalar <const T> : is_scalar<T>
  {
  };

  /**
   *   @brief    Partial specialization for volatile T.
   */
  template <typename T>
  struct  is_scalar <volatile T> : is_scalar<T>
  {
  };

  /**
   *   @brief    Partial specialization for const volatile T.
   */
  template <typename T>
  struct  is_scalar <const volatile T> : is_scalar<T>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_IS_SCALAR_HH_
