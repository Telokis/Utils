/**
*	@file   ProducterCin.hh
*
*	@brief  Defines an stream handler based on std::cin.
*/

#ifndef PRODUCTERCIN_HH_
#define PRODUCTERCIN_HH_

#include "interfaces/IProducterStream.hh"
#include <iostream>

namespace	Utils
{
  /**
  *   @class  ProducterCin  ProducterCin.hh <ProducterCin.hh>
  *
  *   @brief  Defines a stream handler reading from std::cin.
  *
  *   @see        @ref IProducterStream describes a ProducterStream
  *   @see        @ref ProducterString reads from a std::string
  *   @see        @ref ProducterFile reads from a file
  *   @see        #UTILS_PRODUCTER_STREAM_BUFFER_SIZE is an helper tool
  */
  class		ProducterCin : public IProducterStream
  {
  public:
    ProducterCin();                       /**< Constructor */
    ~ProducterCin();                      /**< Destructor */

  public:
    /**
    *   @brief        Function used to get the next available chunk.
    *
    *   @param[out]   ret   A boolean which will be set to false if an error occurred or
    *                       if there were nothing left to read.
    *
    *   @return       The chunk to process. Its size is up to #UTILS_PRODUCTER_STREAM_BUFFER_SIZE
    *                 characters
    */
    std::string	nextString(bool &ret);

    /**
    *   @brief        Function used to test whether std::cin has been closed.
    *
    *   @return       True if std::cin has been closed. False otherwise.
    */
    bool	isEOF() const;
  };

}

#endif // PRODUCTERCIN_HH_
