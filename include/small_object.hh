/**
 *	@file small_object.hh
 *
 *	@brief Contains a shortcut to include all small_objects.
 */

#ifndef UTILS_SMALL_OBJECT_HH_
#define UTILS_SMALL_OBJECT_HH_

#include "small_object/instance_of.hh"
#include "small_object/select_type.hh"
#include "small_object/selector.hh"
#include "small_object/static_parameter.hh"
#include "small_object/static_value.hh"
#include "small_object/type_equal.hh"

#endif // UTILS_SMALL_OBJECT_HH_