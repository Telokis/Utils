#include "ProducterFile.hh"

Utils::ProducterFile::ProducterFile(const std::string &filename)
  : _stream(filename.c_str(), std::ifstream::in)
{
  if (!_stream.is_open())
    throw Exceptions::ProducterRuntimeException("can't open file");
}

Utils::ProducterFile::~ProducterFile()
{
}

std::string	Utils::ProducterFile::nextString(bool &ret)
{
  char		buffer[UTILS_PRODUCTER_STREAM_BUFFER_SIZE] = { 0 };

  ret = false;
  if (_stream.is_open() && _stream.good())
  {
    ret = true;
    _stream.read(buffer, UTILS_PRODUCTER_STREAM_BUFFER_SIZE - 1);
    if (_stream.gcount() < 1)
    {
      ret = false;
      return (std::string(""));
    }
    buffer[_stream.gcount()] = 0;
    return (std::string(buffer));
  }
  return (std::string(""));
}

bool		Utils::ProducterFile::isEOF() const
{
  return (_stream.gcount() < UTILS_PRODUCTER_STREAM_BUFFER_SIZE);
}
