/**
*	@file null_type.hh
*
*	@brief Contains an empty class representing a null type.
*/

#ifndef NULL_TYPE_HH_
#define NULL_TYPE_HH_

namespace Utils
{
	/**
	*	@struct null_type	null_type.hh	<null_type.hh>
	*
	*	@brief	Small object representing a null type.
	*/
	struct null_type
	{
	};
}

#endif // NULL_TYPE_HH_