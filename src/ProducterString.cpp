#include "ProducterString.hh"

Utils::ProducterString::ProducterString(const std::string &string)
  : _stream(string)
{
}

Utils::ProducterString::~ProducterString()
{
}

std::string	Utils::ProducterString::nextString(bool &ret)
{
  std::string	res;

  ret = true;
  res = _stream.substr(0, UTILS_PRODUCTER_STREAM_BUFFER_SIZE);
  _stream.erase(0, UTILS_PRODUCTER_STREAM_BUFFER_SIZE);
  if (res.size() == 0)
  {
    ret = false;
    return (std::string(""));
  }
  return (res);
}

bool		Utils::ProducterString::isEOF() const
{
  return (_stream.size() == 0);
}
