#ifndef DYNAMICLIBRARYWINDOWS_HH_
#define DYNAMICLIBRARYWINDOWS_HH_

#include "interfaces/IDynamicLibrary.hh"
#include "Exceptions.hh"
#include <string>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace	                Utils
{
	/**
	*	@namespace	Utils::Windows
	*	@brief		Contains everything specific to Windows.
	*/
	namespace Windows
	{
		class			            DynamicLibraryWindows : public IDynamicLibrary
		{
		public:
			bool		            load(std::string const &path);
			void		            unload();
			void		            *findFunc(std::string const &name);
			DynamicLibraryWindows(std::string const &path);
			DynamicLibraryWindows();
			~DynamicLibraryWindows();

		private:
			HMODULE					_lib;
		}; // DynamicLibraryWindows
	}
} // Utils

#endif // DYNAMICLIBRARYWINDOWS_HH_
