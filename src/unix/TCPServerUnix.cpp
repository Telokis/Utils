#include "unix/TCPServerUnix.hh"
#include "unix/TCPClientUnix.hh"

Utils::Unix::TCPServerUnix::TCPServerUnix()
  : _socket(-1)
{
}

Utils::Unix::TCPServerUnix::~TCPServerUnix()
{
  if (this->_socket != -1)
  {
    if ((close(_socket)) == -1)
		throw Exceptions::TCPServerRuntimeException("Error while closing socket");
  }
}

Utils::ISocketClient	*Utils::Unix::TCPServerUnix::accept() const
{
  int			clientTCP = -1;
  sockaddr_in		sinClient;
  int			len = sizeof(sinClient);

  if (_socket == -1)
	  throw Exceptions::TCPServerLogicException("Server isn't initialized. Call start first.");
  if ((clientTCP = ::accept(_socket, (struct sockaddr *)&sinClient,
			       (socklen_t *)&len)) < 0)
    throw Exceptions::TCPServerRuntimeException("Unable to accept client");
  ISocketClient		*client = new TCPClientUnix;
  ((TCPClientUnix*)client)->initializeFromAccept(clientTCP);
  return (client);
}

bool			Utils::Unix::TCPServerUnix::start(unsigned short int port)
{
  struct protoent	*protocol;
  sockaddr_in		sin;

  if ((protocol = getprotobyname("TCP")) == NULL)
    return (false);
  if ((this->_socket = socket(AF_INET, SOCK_STREAM, protocol->p_proto)) < 0)
    return (false);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = INADDR_ANY;
  if (bind(this->_socket, (const struct sockaddr *)&sin, sizeof(sin)) < 0)
    return (false);
  if (listen(this->_socket, 100) < 0)
    return (false);
  return (true);
}