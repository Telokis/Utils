#ifndef THREADWINDOWS_HH_
#define THREADWINDOWS_HH_

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <cstdlib>
#include "Exceptions.hh"
#include <string>
#include "interfaces/IThread.hh"

namespace	        Utils
{
	namespace Windows
	{
		class				ThreadWindows : public IThread
		{
		public:
			ThreadWindows(IThreadFunctor *functor, void *arg = NULL);
			ThreadWindows(void(*func)(void *), void *arg = NULL);
			ThreadWindows();
			virtual void	join();
			virtual void	launch(IThreadFunctor *functor, void *arg = NULL);
			virtual void	launch(void(*func)(void *), void *arg = NULL);
			virtual void	launch(const base_functor<void> &f);
			virtual void	terminate();
			~ThreadWindows();

		private:
			HANDLE			_thread;
			bool			_started;

		private:
			struct			  FunctorParams
			{
				FunctorParams(IThreadFunctor *f, void *arg)
					: func(f), param(arg)
				{}
				IThreadFunctor  *func;
				void			  *param;
			}; // FunctorParams

			class		ThreadBindLauncher : public IThreadFunctor
			{
			public:
				ThreadBindLauncher(const base_functor<void> &f);
				~ThreadBindLauncher();
				virtual void	operator()(void *);

			private:
				const base_functor<void> &_f;
			}; // ThreadBindLauncher

			class				ThreadBasicLauncher : public IThreadFunctor
			{
			public:
				ThreadBasicLauncher(void(*f)(void *));
				~ThreadBasicLauncher();
				virtual void	    operator()(void *arg);

			private:
				void(*_f)(void *);
			}; // ThreadBasicLauncher

		private:
			static DWORD WINAPI	_handler(LPVOID arg);
		}; // ThreadWindows
	}
} // Utils

#endif // THREADWINDOWS_HH_
