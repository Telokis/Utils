#include "windows/DynamicLibraryWindows.hh"
#include <iostream>

Utils::Windows::DynamicLibraryWindows::DynamicLibraryWindows(std::string const &path)
	: _lib(NULL)
{
	this->load(path);
}

Utils::Windows::DynamicLibraryWindows::DynamicLibraryWindows()
	: _lib(NULL)
{
}

Utils::Windows::DynamicLibraryWindows::~DynamicLibraryWindows()
{
	this->unload();
}

void	*Utils::Windows::DynamicLibraryWindows::findFunc(std::string const &name)
{
	if (!this->_lib)
		throw Exceptions::DynamicLibraryLogicException("No library loaded");
	return (GetProcAddress(this->_lib, name.c_str()));
}

bool	Utils::Windows::DynamicLibraryWindows::load(std::string const &path)
{
	this->unload();
	this->_lib = LoadLibrary(path.c_str());
	if (!this->_lib)
		return (false);
	return (true);
}

void	Utils::Windows::DynamicLibraryWindows::unload()
{
	int	ret;

	if (this->_lib != NULL)
	{
		ret = FreeLibrary(this->_lib);
		if (ret == 0)
			throw Exceptions::DynamicLibraryRuntimeException("Error while unloading library");
	}
	this->_lib = NULL;
}