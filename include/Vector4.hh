#ifndef VECTOR4_HH_
#define VECTOR4_HH_

#include <iostream>

namespace       Utils
{
  template <typename T>
  class         Vector4
  {
  public:
    Vector4();
    Vector4(T _x, T _y, T _z, T _w);
    Vector4(T a);
    virtual     ~Vector4() {}
    Vector4(const Vector4 &ref);
    Vector4     &operator=(const Vector4 &ref);

  public:
    // Add
    Vector4     &operator+=(const Vector4 &ref);
    Vector4     &operator+=(const T &n);
    Vector4     operator+(const Vector4<T> &b) const;
    Vector4     operator+(const T &b) const;

    // Sub
    Vector4     &operator-=(const Vector4 &ref);
    Vector4     &operator-=(const T &n);
    Vector4     operator-(const Vector4<T> &b) const;
    Vector4     operator-(const T &b) const;

    // Mult
    Vector4	    &operator*=(const Vector4 &ref);
    Vector4	    &operator*=(const T &n);
    Vector4	    operator*(const Vector4<T> &b) const;
    Vector4	    operator*(const T &b) const;

    // Div
    Vector4	    &operator/=(const Vector4 &ref);
    Vector4	    &operator/=(const T &n);
    Vector4	    operator/(const Vector4<T> &b) const;
    Vector4	    operator/(const T &b) const;

    // Compare
    bool	    operator==(const Vector4 &ref) const;
    bool	    operator!=(const Vector4 &ref) const;

  public:
    void	    normalize();
    void	    serialize(std::ostream &) const;

  public:
    static Vector4	normalize(const Vector4 &v);

  public:
    T	        x;
    T	        y;
    union { T z; T wid; };
    union { T w; T hei; };
  }; // Vector4

  template <typename T>
  std::ostream  &operator<<(std::ostream &str, const Utils::Vector4<T> &v)
  {
    v.serialize(str);
    return (str);
  }

  // Useful typedefs for easier use
  typedef	Vector4<float>	        vec4;
  typedef	Vector4<float>	        fvec4;
  typedef	Vector4<int>	        ivec4;
  typedef	Vector4<double>	        dvec4;
  typedef	Vector4<char>	        cvec4;
  typedef	Vector4<unsigned int>	uivec4;
  typedef	Vector4<unsigned char>	ucvec4;
} // Utils

#endif // VECTOR4_HH_
