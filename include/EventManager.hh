#ifndef EVENTMANAGER_HH_
#define EVENTMANAGER_HH_

#include <string>
#include <map>
#include "unique_ptr.hh"
#include "SharedPtr.hh"
#include "Mutex.hh"
#include "non_copyable.hh"
#include "function.hh"

namespace		    Utils
{
  typedef		    function<void(void *, void *)> t_callback;

  template <class T>
  class			    EventManager : public non_copyable
  {
  private:
    class		EventHandler : public non_copyable
    {
    public:
      EventHandler(const std::string &name, t_callback func, void *param)
        : _name(name), _param(param), _func(func)
      {
      }
      ~EventHandler()
      {
      }
      void		call(void *triggerParam)
      {
        this->_func(triggerParam, this->_param);
      }

    private:
      std::string	_name;
      void		*_param;
      t_callback	_func;
    }; // EventHandler

  public:
    EventManager()
      : _mutex(newMutex())
    {
    }
    ~EventManager()
    {
    }

  public:
    void		    on(const T &event, const std::string &id, t_callback callback, void *param)
    {
      Utils::ScopedMutex(this->_mutex);

      this->cancel(event, id);
      this->_events[event][id] = new EventHandler(id, callback, param);
    }
    void		    cancel(const T &event, const std::string &id)
    {
      Utils::ScopedMutex(this->_mutex);

      if (this->_events.find(event) != this->_events.end() &&
        this->_events[event].find(id) != this->_events[event].end())
      {
        this->_events[event].erase(id);
      }
    }
    void		    trigger(const T &event, void *triggerParam)
    {
      Utils::ScopedMutex(this->_mutex);
      typename std::map<std::string, SharedPtr<EventHandler> >::const_iterator	it;
      typename std::map<std::string, SharedPtr<EventHandler> >::const_iterator	end;

      end = this->_events[event].end();
      for (it = this->_events[event].begin();
        it != end;
        ++it)
      {
        it->second->call(triggerParam);
      }
    }

  private:
    typedef		std::map<T, std::map<std::string, SharedPtr<typename EventManager::EventHandler> > >	t_collection;
    t_collection	_events;
    unique_ptr<IMutex>		_mutex;
  }; // EventManager

} // Utils

#endif // EVENTMANAGER_HH_
