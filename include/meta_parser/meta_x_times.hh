#ifndef META_X_TIMES_HH_
#define META_X_TIMES_HH_

namespace	Utils
{

  //////////////////////////////////////////
  //                                      //
  // -------------- WARNING ------------- //
  // Keep track of the context !!!        //
  //                                      //
  //////////////////////////////////////////

  // +-------------------------+
  // | For a pointer to method |
  // +-------------------------+

  // For zero parameter
  template <class P>
  inline bool	x_times(unsigned int n, bool (P::*func)(), P &parser)
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!((parser.*func)()))
	  return (false);
      }
    return (true);
  }

  // For one parameter
  template <class P, class A0>
  inline bool	x_times(unsigned int n, bool (P::*func)(A0), P &parser, A0 arg0)
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!((parser.*func)(arg0)))
	  return (false);
      }
    return (true);
  }

  // For two parameters
  template <class P, class A0, class A1>
  inline bool	x_times(unsigned int n, bool (P::*func)(A0, A1), P &parser, A0 arg0, A1 arg1)
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!((parser.*func)(arg0, arg1)))
	  return (false);
      }
    return (true);
  }

  // +---------------------------+
  // | For a pointer to function |
  // +---------------------------+

  // For zero parameter
  inline bool	x_times(unsigned int n, bool (*func)())
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!(func()))
	  return (false);
      }
    return (true);
  }

  // For one parameter
  template <class A0>
  inline bool	x_times(unsigned int n, bool (*func)(A0), A0 arg0)
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!(func(arg0)))
	  return (false);
      }
    return (true);
  }

  // For two parameters
  template <class A0, class A1>
  inline bool	x_times(unsigned int n, bool (*func)(A0, A1), A0 arg0, A1 arg1)
  {
    for (unsigned int i = 0; i < n; ++i)
      {
	if (!(func(arg0, arg1)))
	  return (false);
      }
    return (true);
  }

}

#endif // META_X_TIMES_HH_
