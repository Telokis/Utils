#include "unix/ThreadUnix.hh"
#include "SharedPtr.hh"
#include <iostream>

Utils::Unix::ThreadUnix::ThreadUnix()
  : _started(false)
{
}

Utils::Unix::ThreadUnix::ThreadUnix(IThreadFunctor *functor, void *arg)
  : _started(false)
{
  this->launch(functor, arg);
}

Utils::Unix::ThreadUnix::ThreadUnix(void(*func)(void *), void *arg)
  : _started(false)
{
  this->launch(func, arg);
}

Utils::Unix::ThreadUnix::~ThreadUnix()
{
}

Utils::Unix::ThreadUnix::ThreadBasicLauncher::ThreadBasicLauncher(void(*f)(void *))
  : _f(f)
{
}

Utils::Unix::ThreadUnix::ThreadBasicLauncher::~ThreadBasicLauncher()
{
}

void	Utils::Unix::ThreadUnix::ThreadBasicLauncher::operator()(void *arg)
{
  _f(arg);
}

Utils::Unix::ThreadUnix::ThreadBindLauncher::ThreadBindLauncher(const Utils::base_functor<void> &f)
  : _f(f)
{
}

Utils::Unix::ThreadUnix::ThreadBindLauncher::~ThreadBindLauncher()
{
}

void	Utils::Unix::ThreadUnix::ThreadBindLauncher::operator()(void *)
{
  _f();
}

void		*Utils::Unix::ThreadUnix::_handler(void *arg)
{
  FunctorParams	*params = static_cast<FunctorParams *>(arg);

  (*params->func)(params->param);
  return (NULL);
}

void		Utils::Unix::ThreadUnix::launch(IThreadFunctor *functor, void *arg)
{
  FunctorParams	*params = new FunctorParams(functor, arg);

  if (_started == true)
	  throw Exceptions::ThreadLogicException("Thread already launched");
  if ((pthread_create(&this->_thread, NULL, _handler, params)) != 0)
	  throw Exceptions::ThreadRuntimeException("Could not initialize thread");
  _started = true;
}

void			Utils::Unix::ThreadUnix::launch(void(*func)(void *), void *arg)
{
  ThreadBasicLauncher	*_threadBasicLauncher = new ThreadBasicLauncher(func);

  this->launch(_threadBasicLauncher, arg);
}

void			Utils::Unix::ThreadUnix::launch(const base_functor<void> &f)
{
  ThreadBindLauncher	*_threadBindLauncher = new ThreadBindLauncher(f);

  this->launch(_threadBindLauncher);
}

void                    Utils::Unix::ThreadUnix::terminate()
{
  pthread_kill(_thread, SIGTERM);
}


void		Utils::Unix::ThreadUnix::join()
{
  if (_started == false)
	  throw Exceptions::ThreadLogicException("Thread wasn't running");
  if (pthread_join(this->_thread, NULL) != 0)
	  throw Exceptions::ThreadLogicException("Could not join thread");
}