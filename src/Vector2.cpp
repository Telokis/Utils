#include "Vector2.hh"
#include <math.h>

template <typename T>
Utils::Vector2<T>::Vector2()
  : x(0), y(0)
{
}

template <typename T>
Utils::Vector2<T>::Vector2(T _x, T _y)
  : x(_x), y(_y)
{
}

template <typename T>
Utils::Vector2<T>::Vector2(T a)
  : x(a), y(a)
{
}

template <typename T>
Utils::Vector2<T>::Vector2(const Vector2<T> &ref)
  : x(ref.x), y(ref.y)
{
}

template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator=(const Vector2<T> &ref)
{
  if (this != &ref)
  {
    this->x = ref.x;
    this->y = ref.y;
  }
  return (*this);
}

template <typename T>
void			Utils::Vector2<T>::normalize()
{
  T			    d;

  d = static_cast<T>(sqrt(this->x * this->x + this->y * this->y));
  this->x /= d;
  this->y /= d;
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::normalize(const Vector2<T> &v)
{
  Vector2<T>		res(v);

  res.normalize();
  return (res);
}

template <typename T>
void	Utils::Vector2<T>::serialize(std::ostream &os) const
{
  os << "{ " << x << ", " << y << " }";
}

// ---------- Add ---------
template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator+=(const Vector2<T> &ref)
{
  this->x += ref.x;
  this->y += ref.y;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator+=(const T &n)
{
  this->x += n;
  this->y += n;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator+(const Vector2<T> &b) const
{
  Vector2<T>	res(*this);

  res += b;
  return (res);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator+(const T &b) const
{
  Vector2<T>	res(*this);

  res += b;
  return (res);
}

// ---------- Sub ----------
template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator-=(const Vector2<T> &ref)
{
  this->x -= ref.x;
  this->y -= ref.y;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator-=(const T &n)
{
  this->x -= n;
  this->y -= n;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator-(const Vector2<T> &b) const
{
  Vector2<T>	res(*this);

  res -= b;
  return (res);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator-(const T &b) const
{
  Vector2<T>	res(*this);

  res -= b;
  return (res);
}

// ---------- Mult ----------
template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator*=(const Vector2<T> &ref)
{
  this->x *= ref.x;
  this->y *= ref.y;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator*=(const T &n)
{
  this->x *= n;
  this->y *= n;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator*(const Vector2<T> &b) const
{
  Vector2<T>	res(*this);

  res *= b;
  return (res);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator*(const T &b) const
{
  Vector2<T>	res(*this);

  res *= b;
  return (res);
}

// ---------- Div ----------
template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator/=(const Vector2<T> &ref)
{
  this->x /= ref.x;
  this->y /= ref.y;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	&Utils::Vector2<T>::operator/=(const T &n)
{
  this->x /= n;
  this->y /= n;
  return (*this);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator/(const Vector2<T> &b) const
{
  Vector2<T>	res(*this);

  res /= b;
  return (res);
}

template <typename T>
Utils::Vector2<T>	Utils::Vector2<T>::operator/(const T &b) const
{
  Vector2<T>	res(*this);

  res /= b;
  return (res);
}

// ---------- Compare ----------
template <typename T>
bool	Utils::Vector2<T>::operator==(const Vector2 &ref) const
{
  return (x == ref.x && y == ref.y);
}

template <typename T>
bool	Utils::Vector2<T>::operator!=(const Vector2 &ref) const
{
  return (!(*this == ref));
}

template class Utils::Vector2 < int > ;
template class Utils::Vector2 < float > ;
template class Utils::Vector2 < double > ;
template class Utils::Vector2 < char > ;
template class Utils::Vector2 < unsigned int > ;
template class Utils::Vector2 < unsigned char > ;
