#ifndef SOCKETCLIENTWINDOWS_HH_
#define SOCKETCLIENTWINDOWS_HH_

#include <string>
#include <Winsock2.h>
#include <Mswsock.h>
#include "Exceptions.hh"
#include "interfaces/ISocketClient.hh"

namespace					Utils
{
	namespace Windows
	{
		class						TCPClientWindows : public ISocketClient
		{
			friend class			TCPServerWindows;
		public:
			TCPClientWindows();
			~TCPClientWindows();

		public:
			virtual size_t			write(const void *buf, size_t len) const;
			virtual size_t		    read(void *buf, size_t len);
			virtual bool			connect(const std::string &ip, unsigned short int port);

		private:
			void					initializeFromAccept(SOCKET &sock);

		private:
			SOCKET					_socket;
		}; // TCPClientWindows
	}
} // Utils

#endif // SOCKETCLIENTWINDOWS_HH_
