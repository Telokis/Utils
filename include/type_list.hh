/**
*   @file   type_list.hh
*
*   @brief  Describes the type_list class.
*/

#ifndef UTILS_TYPE_LIST_HH_
#define UTILS_TYPE_LIST_HH_

namespace Utils
{
  /**
  *   @struct   type_list type_list.hh  <type_list.hh>
  *
  *   @brief    Powerful class able to hold an infinite amout of different types.
  *
  *   This class is powerful because @a T can be another @ref type_list itself containing
  *       another @ref type_list itself containing another and so on.
  *   @code
  *   type_list<int, type_list<float, type_list<char, null_type> > >; // A type_list holding int, float, char.
  *   @endcode
  *
  *   @tparam   H         The head type.
  *   @tparam   T         The tail type. Can be @ref null_type to end the @ref type_list.
  */
  template <typename H, typename T>
  struct    type_list
  {
    typedef H head;
    typedef T tail;
  };
}

#ifdef _DOXYGEN
#define UTILS_DO_NOT_USE_MACROS_FOR_TYPE_LIST
#endif
/**
*   @def      UTILS_DO_NOT_USE_MACROS_FOR_TYPE_LIST
*   @ingroup  Macros
*   @brief    Defining this macro will prevent Utils from generating (and using)
*               macros to create type_lists. It could decrease compile time to define it.
*/
#if !defined(UTILS_DO_NOT_USE_MACROS_FOR_TYPE_LIST) || defined(_DOXYGEN)

  /**
  *   @def      UTILS_TYPE_LIST_01(T)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 1 type.
  */
  #define UTILS_TYPE_LIST_01(T) \
    (Utils::type_list<T, null_type>)

  /**
  *   @def      UTILS_TYPE_LIST_02(T1, T2)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 2 types.
  */
  #define UTILS_TYPE_LIST_02(T1, T2) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_01(T2)>)

  /**
  *   @def      UTILS_TYPE_LIST_03(T1, T2, T3)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 3 types.
  */
  #define UTILS_TYPE_LIST_03(T1, T2, T3) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_02(T2, T3)>)

  /**
  *   @def      UTILS_TYPE_LIST_04(T1, T2, T3, T4)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 4 types.
  */
  #define UTILS_TYPE_LIST_04(T1, T2, T3, T4) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_03(T2, T3, T4)>)

  /**
  *   @def      UTILS_TYPE_LIST_05(T1, T2, T3, T4, T5)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 5 types.
  */
  #define UTILS_TYPE_LIST_05(T1, T2, T3, T4, T5) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_04(T2, T3, T4, T5)>)

  /**
  *   @def      UTILS_TYPE_LIST_06(T1, T2, T3, T4, T5, T6)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 6 types.
  */
  #define UTILS_TYPE_LIST_06(T1, T2, T3, T4, T5, T6) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_05(T2, T3, T4, T5, T6)>)

  /**
  *   @def      UTILS_TYPE_LIST_07(T1, T2, T3, T4, T5, T6, T7)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 7 types.
  */
  #define UTILS_TYPE_LIST_07(T1, T2, T3, T4, T5, T6, T7) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_06(T2, T3, T4, T5, T6, T7)>)

  /**
  *   @def      UTILS_TYPE_LIST_08(T1, T2, T3, T4, T5, T6, T7, T8)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 8 types.
  */
  #define UTILS_TYPE_LIST_08(T1, T2, T3, T4, T5, T6, T7, T8) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_07(T2, T3, T4, T5, T6, T7, T8)>)

  /**
  *   @def      UTILS_TYPE_LIST_09(T1, T2, T3, T4, T5, T6, T7, T8, T9)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 9 types.
  */
  #define UTILS_TYPE_LIST_09(T1, T2, T3, T4, T5, T6, T7, T8, T9) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_08(T2, T3, T4, T5, T6, T7, T8, T9)>)

  /**
  *   @def      UTILS_TYPE_LIST_10(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 10 types.
  */
  #define UTILS_TYPE_LIST_10(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_09(T2, T3, T4, T5, T6, T7, T8, T9, T10)>)

  /**
  *   @def      UTILS_TYPE_LIST_11(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 11 types.
  */
  #define UTILS_TYPE_LIST_11(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_10(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11)>)

  /**
  *   @def      UTILS_TYPE_LIST_12(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 12 types.
  */
  #define UTILS_TYPE_LIST_12(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_11(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12)>)

  /**
  *   @def      UTILS_TYPE_LIST_13(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 13 types.
  */
  #define UTILS_TYPE_LIST_13(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_12(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13)>)

  /**
  *   @def      UTILS_TYPE_LIST_14(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 14 types.
  */
  #define UTILS_TYPE_LIST_14(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_13(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14)>)

  /**
  *   @def      UTILS_TYPE_LIST_15(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15)
  *   @ingroup  Macros
  *   @brief    Defines a type_list holding 15 types.
  */
  #define UTILS_TYPE_LIST_15(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15) \
    (Utils::type_list<T1, UTILS_TYPE_LIST_14(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15)>)

#endif // !defined(UTILS_DO_NOT_USE_MACROS_FOR_TYPE_LIST)

#endif // UTILS_TYPE_LIST_HH_