#ifndef TCPCLIENT_HH_
#define TCPCLIENT_HH_

#include "interfaces/ISocketClient.hh"

#if defined(_WIN32) || defined(WIN32) // WINDOWS
#include "windows/TCPClientWindows.hh"

namespace Utils
{
	inline ISocketClient	*newTcpClient()
	{
		return (new Windows::TCPClientWindows);
	}
}

#elif defined(__unix) // UNIX
#include "unix/TCPClientUnix.hh"

namespace Utils
{
	inline ISocketClient	*newTcpClient()
	{
		return (new Unix::TCPClientUnix);
	}
}

#else
#error "Unknown platform"
#endif // Platform recognition

#endif // TCPCLIENT_HH_
