/**
*   @file   meta_parser.hh
*
*   @brief  This file is a shortcut for including Utils::MetaParser.
*/

#ifndef META_PARSER_HH__INCLUDE
#define META_PARSER_HH__INCLUDE

#include "meta_parser/MetaParser.hh"

#endif // META_PARSER_HH__INCLUDE