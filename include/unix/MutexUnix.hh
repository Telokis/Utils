#ifndef MUTEXUNIX_HH_
#define MUTEXUNIX_HH_

#include "interfaces/ILockable.hh"
#include <string>
#include "Exceptions.hh"
#include <pthread.h>
#include "interfaces/IMutex.hh"

namespace			    Utils
{
	namespace	Unix
	{
		class	       			MutexUnix : public IMutex
		{
		public:
			MutexUnix();
			~MutexUnix();
			virtual void		lock();
			virtual void		unlock();
			virtual bool		try_lock();
			virtual bool		locked() const;

		private:
			MutexUnix			&operator=(const MutexUnix &);
			MutexUnix(const MutexUnix &);
			pthread_mutex_t		_mutex;
			bool			    _locked;
		}; // MutexUnix
	}
} // Utils

#endif // MUTEXUNIX_HH_
