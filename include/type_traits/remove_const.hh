/**
*   @file   remove_const.hh
*   @brief  Describes Utils::remove_const.
*/

#ifndef UTILS_REMOVE_CONST_HH_
#define UTILS_REMOVE_CONST_HH_

#include "small_object/instance_of.hh"

namespace Utils
{
  /**
  *   @struct   remove_const  remove_const.hh   <type_traits.hh>
  *   @brief    removes a const qualifier from @a T.
  *
  *   remove_const<const T>::type == T.
  *
  *   Examples :
  *   @code
  *   remove_const<int>::type;           // int
  *   remove_const<int const>::type;     // int
  *   remove_const<const int &>::type;   // const int & --> const is not at top level.
  *   remove_const<const int *>::type;   // const int * --> content is const, not pointer.
  *   remove_const<int * const>::type;   // int *
  *   @endcode
  *
  *   @tparam   T The type to remove constness from.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  remove_const : instance_of<T>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for const T.
  */
  template <typename T>
  struct  remove_const <const T> : instance_of<T>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_REMOVE_CONST_HH_