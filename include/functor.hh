#ifndef FUNCTOR_HH_
#define FUNCTOR_HH_

#include "SharedPtr.hh"

namespace	Utils
{

  template <class R>
  struct	base_functor
  {
    virtual R	operator()() const = 0;
  };

  // +---------------------------+
  // | For a pointer to function |
  // +---------------------------+
  // For zero parameter
  template <class R>
  struct	ffunctor_0 : public base_functor<R>
  {
    ffunctor_0(R (*f)())
    : _f(f)
    {
    }
    R	operator()() const
    {
      return (_f());
    }

  private:
    R (*_f)();
  };

  // For one parameter
  template <class R, class A0>
  struct	ffunctor_1 : public base_functor<R>
  {
    ffunctor_1(R (*f)(A0), A0 a0)
      : _f(f), _a0(a0)
    {
    }
    R	operator()() const
    {
      return (_f(_a0));
    }

  private:
    R (*_f)(A0);
    A0	_a0;
  };

  // For two parameters
  template <class R, class A0, class A1>
  struct	ffunctor_2 : public base_functor<R>
  {
    ffunctor_2(R (*f)(A0, A1), A0 a0, A1 a1)
      : _f(f), _a0(a0), _a1(a1)
    {
    }
    R	operator()() const
    {
      return (_f(_a0, _a1));
    }

  private:
    R (*_f)(A0, A1);
    A0	_a0;
    A1	_a1;
  };

  // For three parameters
  template <class R, class A0, class A1, class A2>
  struct	ffunctor_3 : public base_functor<R>
  {
    ffunctor_3(R (*f)(A0, A1, A2), A0 a0, A1 a1, A2 a2)
      : _f(f), _a0(a0), _a1(a1), _a2(a2)
    {
    }
    R	operator()() const
    {
      return (_f(_a0, _a1, _a2));
    }

  private:
    R (*_f)(A0, A1, A2);
    A0	_a0;
    A1	_a1;
    A2	_a2;
  };


  // +-------------------------+
  // | For a pointer to method |
  // +-------------------------+
  // For zero parameter
  template <class R, class C>
  struct	mfunctor_0 : public base_functor<R>
  {
    mfunctor_0(R (C::*f)(), C *c)
      : _f(f), _c(c)
    {
    }
    R	operator()() const
    {
      return ((_c->*_f)());
    }

  private:
    R (C::*_f)();
    C	*_c;
  };

  // For one parameter
  template <class R, class C, class A0>
  struct	mfunctor_1 : public base_functor<R>
  {
    mfunctor_1(R (C::*f)(A0), C *c, A0 a0)
      : _f(f), _c(c), _a0(a0)
    {
    }
    R	operator()() const
    {
      return ((_c->*_f)(_a0));
    }

  private:
    R (C::*_f)(A0);
    C	*_c;
    A0	_a0;
  };

  // For two parameters
  template <class R, class C, class A0, class A1>
  struct	mfunctor_2 : public base_functor<R>
  {
    mfunctor_2(R (C::*f)(A0, A1), C *c, A0 a0, A1 a1)
      : _f(f), _c(c), _a0(a0), _a1(a1)
    {
    }
    R	operator()() const
    {
      return ((_c->*_f)(_a0, _a1));
    }

  private:
    R (C::*_f)(A0, A1);
    C	*_c;
    A0	_a0;
    A1	_a1;
  };

  // For three parameters
  template <class R, class C, class A0, class A1, class A2>
  struct	mfunctor_3 : public base_functor<R>
  {
    mfunctor_3(R (C::*f)(A0, A1, A2), C *c, A0 a0, A1 a1, A2 a2)
      : _f(f), _c(c), _a0(a0), _a1(a1), _a2(a2)
    {
    }
    R	operator()() const
    {
      return ((_c->*_f)(_a0, _a1, _a2));
    }

  private:
    R (C::*_f)(A0, A1, A2);
    C	*_c;
    A0	_a0;
    A1	_a1;
    A2	_a2;
  };


  // +------------------------------------------+
  // | For a pointer to method with a SharedPtr |
  // +------------------------------------------+
  // For zero parameter
  template <class R, class C>
  struct	sfunctor_0 : public base_functor<R>
  {
    sfunctor_0(R (C::*f)(), const SharedPtr<C> &c)
      : _f(f), _c(c)
    {
    }
    R	operator()() const
    {
      return ((_c.get()->*_f)());
    }

  private:
    R (C::*_f)();
    SharedPtr<C>	_c;
  };

  // For one parameter
  template <class R, class C, class A0>
  struct	sfunctor_1 : public base_functor<R>
  {
    sfunctor_1(R (C::*f)(A0), const SharedPtr<C> &c, A0 a0)
      : _f(f), _c(c), _a0(a0)
    {
    }
    R	operator()() const
    {
      return ((_c.get()->*_f)(_a0));
    }

  private:
    R (C::*_f)(A0);
    SharedPtr<C>	_c;
    A0	_a0;
  };

  // For two parameters
  template <class R, class C, class A0, class A1>
  struct	sfunctor_2 : public base_functor<R>
  {
    sfunctor_2(R (C::*f)(A0, A1), const SharedPtr<C> &c, A0 a0, A1 a1)
      : _f(f), _c(c), _a0(a0), _a1(a1)
    {
    }
    R	operator()() const
    {
      return ((_c.get()->*_f)(_a0, _a1));
    }

  private:
    R (C::*_f)(A0, A1);
    SharedPtr<C>	_c;
    A0	_a0;
    A1	_a1;
  };

  // For three parameters
  template <class R, class C, class A0, class A1, class A2>
  struct	sfunctor_3 : public base_functor<R>
  {
    sfunctor_3(R (C::*f)(A0, A1, A2), const SharedPtr<C> &c, A0 a0, A1 a1, A2 a2)
      : _f(f), _c(c), _a0(a0), _a1(a1), _a2(a2)
    {
    }
    R	operator()() const
    {
      return ((_c.get()->*_f)(_a0, _a1, _a2));
    }

  private:
    R (C::*_f)(A0, A1, A2);
    SharedPtr<C>	_c;
    A0	_a0;
    A1	_a1;
    A2	_a2;
  };

} // Utils

#endif // FUNCTOR_HH_
