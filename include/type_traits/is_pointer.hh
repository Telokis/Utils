/**
*   @file   is_pointer.hh
*   @brief  Describes Utils::is_pointer.
*/

#ifndef UTILS_IS_POINTER_HH_
#define UTILS_IS_POINTER_HH_

#include "small_object/selector.hh"

namespace Utils
{
  /**
  *   @struct   is_pointer  is_pointer.hh   <type_traits.hh>
  *   @brief    Tells whether @a T is pointer or not.
  *
  *   If @a T is pointer, is_pointer<T>::value == true.
  *
  *   @tparam   T The type to check for pointerness.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  is_pointer : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for T*.
  */
  template <typename T>
  struct  is_pointer <T *> : true_type
  {
  };

  /**
  *   @brief    Partial specialization for T * const.
  */
  template <typename T>
  struct  is_pointer <T * const> : true_type
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_IS_POINTER_HH_