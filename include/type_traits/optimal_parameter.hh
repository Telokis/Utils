#ifndef OPTIMAL_PARAMETER_HH_
#define OPTIMAL_PARAMETER_HH_

#include "is_scalar.hh"

namespace Utils
{
  template <typename T>
  struct	optimal_parameter
    : instance_of<T>
  {
  };

  template <typename T>
  struct	optimal_parameter <T *>
    : instance_of<T *>
  {
  };

  template <typename T>
  struct	optimal_parameter <T &>
    : instance_of<T &>
  {
  };

  template <typename T>
  struct	optimal_parameter <const T&>
    : instance_of<const T &>
  {
  };

  template <typename T>
  struct	optimal_parameter <const T>
    : instance_of<const T &>
  {
  };
}
#endif // OPTIMAL_PARAMETER_HH_
