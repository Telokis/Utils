/**
*   @file   instance_of.hh
*
*   @brief  Contains definition of class Utils::instance_of.
*/

#ifndef UTILS_INSTANCE_OF_HH_
#define UTILS_INSTANCE_OF_HH_

namespace Utils
{
  /**
  *	@struct instance_of	instance_of.hh	<small_object.hh>
  *
  *	@tparam	T	The type that @ref instance_of has to represent.
  *
  *	@brief	Small object used to represent a type.
  *
  *	It can be used to declare global const variables like this :
  *	@code{.cpp} const instance_of<int> T_INT = 0; // Represents the int type @endcode
  */
  template <typename T>
  struct instance_of
  {
    typedef T type;					  /**< Typedef on @a T for easier reuse */

    /**
    *	@brief Constructor used to declare @ref instance_of easier.
    *	Using "= 0" instead of nothing will disable the "variable unused" compiler warning.
    *
    *	@param	i	A fake parameter that allows us to write a direct assignment.
    */
    instance_of(int = 0)
    {
    }
  };
}

#endif // UTILS_INSTANCE_OF_HH_
