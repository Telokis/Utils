/**
*   @file   add_const.hh
*   @brief  Describes Utils::add_const.
*/

#ifndef UTILS_ADD_CONST_HH_
#define UTILS_ADD_CONST_HH_

#include "small_object/instance_of.hh"

namespace Utils
{
  /**
  *   @struct   add_const  add_const.hh   <type_traits.hh>
  *   @brief    Adds a const qualifier to @a T.
  *
  *   add_const<T>::type == const T.
  *
  *   Examples :
  *   @code
  *   add_const<int>::type;           // int const
  *   add_const<int &>::type;         // int &
  *   add_const<int *>::type;         // int * const
  *   add_const<int const>::type;     // int const
  *   add_const<const int *>::type;   // const int *const
  *   @endcode
  *
  *   @tparam   T The type to add constness on.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  add_const : instance_of<const T>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for const T.
  */
  template <typename T>
  struct  add_const <const T> : instance_of<const T>
  {
  };

  /**
  *   @brief    Partial specialization for T&.
  */
  template <typename T>
  struct  add_const <T&> : instance_of<T&>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_ADD_CONST_HH_