#ifndef MUTEX_HH_
#define MUTEX_HH_

#include "interfaces/IMutex.hh"
#include "ScopedMutex.hh"

#if defined(_WIN32) || defined(WIN32) // WINDOWS
#include "windows/MutexWindows.hh"

namespace Utils
{
	inline IMutex	*newMutex()
	{
		return (new Windows::MutexWindows);
	}
}

#elif defined(__unix) // UNIX
#include "unix/MutexUnix.hh"

namespace Utils
{
	inline IMutex	*newMutex()
	{
		return (new Unix::MutexUnix);
	}
}

#else
#error "Unknown platform"
#endif // Platform recognition

#endif // MUTEX_HH_
