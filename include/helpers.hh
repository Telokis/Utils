#ifndef HELPERS_HH_
#define HELPERS_HH_

#define UTILS_GENERATE_1(expr) expr ## 1
#define UTILS_GENERATE_2(expr) UTILS_GENERATE_1(expr), expr ## 2
#define UTILS_GENERATE_3(expr) UTILS_GENERATE_2(expr), expr ## 3
#define UTILS_GENERATE_4(expr) UTILS_GENERATE_3(expr), expr ## 4
#define UTILS_GENERATE_5(expr) UTILS_GENERATE_4(expr), expr ## 5
#define UTILS_GENERATE_6(expr) UTILS_GENERATE_5(expr), expr ## 6
#define UTILS_GENERATE_7(expr) UTILS_GENERATE_6(expr), expr ## 7
#define UTILS_GENERATE_8(expr) UTILS_GENERATE_7(expr), expr ## 8
#define UTILS_GENERATE_9(expr) UTILS_GENERATE_8(expr), expr ## 9
#define UTILS_GENERATE_10(expr) UTILS_GENERATE_9(expr), expr ## 10
#define UTILS_GENERATE_11(expr) UTILS_GENERATE_10(expr), expr ## 11
#define UTILS_GENERATE_12(expr) UTILS_GENERATE_11(expr), expr ## 12
#define UTILS_GENERATE_13(expr) UTILS_GENERATE_12(expr), expr ## 13
#define UTILS_GENERATE_14(expr) UTILS_GENERATE_13(expr), expr ## 14
#define UTILS_GENERATE_15(expr) UTILS_GENERATE_14(expr), expr ## 15
#define UTILS_GENERATE_16(expr) UTILS_GENERATE_15(expr), expr ## 16
#define UTILS_GENERATE_17(expr) UTILS_GENERATE_16(expr), expr ## 17
#define UTILS_GENERATE_18(expr) UTILS_GENERATE_17(expr), expr ## 18
#define UTILS_GENERATE_19(expr) UTILS_GENERATE_18(expr), expr ## 19
#define UTILS_GENERATE_20(expr) UTILS_GENERATE_19(expr), expr ## 20

#define UTILS_GENERATE_DOUBLE_1(expr1, expr2) expr1 ## 1 expr2 ## 1
#define UTILS_GENERATE_DOUBLE_2(expr1, expr2) UTILS_GENERATE_DOUBLE_1(expr1, expr2), expr1 ## 2 expr2 ## 2
#define UTILS_GENERATE_DOUBLE_3(expr1, expr2) UTILS_GENERATE_DOUBLE_2(expr1, expr2), expr1 ## 3 expr2 ## 3
#define UTILS_GENERATE_DOUBLE_4(expr1, expr2) UTILS_GENERATE_DOUBLE_3(expr1, expr2), expr1 ## 4 expr2 ## 4
#define UTILS_GENERATE_DOUBLE_5(expr1, expr2) UTILS_GENERATE_DOUBLE_4(expr1, expr2), expr1 ## 5 expr2 ## 5
#define UTILS_GENERATE_DOUBLE_6(expr1, expr2) UTILS_GENERATE_DOUBLE_5(expr1, expr2), expr1 ## 6 expr2 ## 6
#define UTILS_GENERATE_DOUBLE_7(expr1, expr2) UTILS_GENERATE_DOUBLE_6(expr1, expr2), expr1 ## 7 expr2 ## 7
#define UTILS_GENERATE_DOUBLE_8(expr1, expr2) UTILS_GENERATE_DOUBLE_7(expr1, expr2), expr1 ## 8 expr2 ## 8
#define UTILS_GENERATE_DOUBLE_9(expr1, expr2) UTILS_GENERATE_DOUBLE_8(expr1, expr2), expr1 ## 9 expr2 ## 9
#define UTILS_GENERATE_DOUBLE_10(expr1, expr2) UTILS_GENERATE_DOUBLE_9(expr1, expr2), expr1 ## 10 expr2 ## 10
#define UTILS_GENERATE_DOUBLE_11(expr1, expr2) UTILS_GENERATE_DOUBLE_10(expr1, expr2), expr1 ## 11 expr2 ## 11
#define UTILS_GENERATE_DOUBLE_12(expr1, expr2) UTILS_GENERATE_DOUBLE_11(expr1, expr2), expr1 ## 12 expr2 ## 12
#define UTILS_GENERATE_DOUBLE_13(expr1, expr2) UTILS_GENERATE_DOUBLE_12(expr1, expr2), expr1 ## 13 expr2 ## 13
#define UTILS_GENERATE_DOUBLE_14(expr1, expr2) UTILS_GENERATE_DOUBLE_13(expr1, expr2), expr1 ## 14 expr2 ## 14
#define UTILS_GENERATE_DOUBLE_15(expr1, expr2) UTILS_GENERATE_DOUBLE_14(expr1, expr2), expr1 ## 15 expr2 ## 15
#define UTILS_GENERATE_DOUBLE_16(expr1, expr2) UTILS_GENERATE_DOUBLE_15(expr1, expr2), expr1 ## 16 expr2 ## 16
#define UTILS_GENERATE_DOUBLE_17(expr1, expr2) UTILS_GENERATE_DOUBLE_16(expr1, expr2), expr1 ## 17 expr2 ## 17
#define UTILS_GENERATE_DOUBLE_18(expr1, expr2) UTILS_GENERATE_DOUBLE_17(expr1, expr2), expr1 ## 18 expr2 ## 18
#define UTILS_GENERATE_DOUBLE_19(expr1, expr2) UTILS_GENERATE_DOUBLE_18(expr1, expr2), expr1 ## 19 expr2 ## 19
#define UTILS_GENERATE_DOUBLE_20(expr1, expr2) UTILS_GENERATE_DOUBLE_19(expr1, expr2), expr1 ## 20 expr2 ## 20

#define UTILS_INTERPOSE_1(expr1, expr2) expr1 ## 1 expr2
#define UTILS_INTERPOSE_2(expr1, expr2) UTILS_INTERPOSE_1(expr1, expr2), expr1 ## 2 expr2
#define UTILS_INTERPOSE_3(expr1, expr2) UTILS_INTERPOSE_2(expr1, expr2), expr1 ## 3 expr2
#define UTILS_INTERPOSE_4(expr1, expr2) UTILS_INTERPOSE_3(expr1, expr2), expr1 ## 4 expr2
#define UTILS_INTERPOSE_5(expr1, expr2) UTILS_INTERPOSE_4(expr1, expr2), expr1 ## 5 expr2
#define UTILS_INTERPOSE_6(expr1, expr2) UTILS_INTERPOSE_5(expr1, expr2), expr1 ## 6 expr2
#define UTILS_INTERPOSE_7(expr1, expr2) UTILS_INTERPOSE_6(expr1, expr2), expr1 ## 7 expr2
#define UTILS_INTERPOSE_8(expr1, expr2) UTILS_INTERPOSE_7(expr1, expr2), expr1 ## 8 expr2
#define UTILS_INTERPOSE_9(expr1, expr2) UTILS_INTERPOSE_8(expr1, expr2), expr1 ## 9 expr2
#define UTILS_INTERPOSE_10(expr1, expr2) UTILS_INTERPOSE_9(expr1, expr2), expr1 ## 10 expr2
#define UTILS_INTERPOSE_11(expr1, expr2) UTILS_INTERPOSE_10(expr1, expr2), expr1 ## 11 expr2
#define UTILS_INTERPOSE_12(expr1, expr2) UTILS_INTERPOSE_11(expr1, expr2), expr1 ## 12 expr2
#define UTILS_INTERPOSE_13(expr1, expr2) UTILS_INTERPOSE_12(expr1, expr2), expr1 ## 13 expr2
#define UTILS_INTERPOSE_14(expr1, expr2) UTILS_INTERPOSE_13(expr1, expr2), expr1 ## 14 expr2
#define UTILS_INTERPOSE_15(expr1, expr2) UTILS_INTERPOSE_14(expr1, expr2), expr1 ## 15 expr2
#define UTILS_INTERPOSE_16(expr1, expr2) UTILS_INTERPOSE_15(expr1, expr2), expr1 ## 16 expr2
#define UTILS_INTERPOSE_17(expr1, expr2) UTILS_INTERPOSE_16(expr1, expr2), expr1 ## 17 expr2
#define UTILS_INTERPOSE_18(expr1, expr2) UTILS_INTERPOSE_17(expr1, expr2), expr1 ## 18 expr2
#define UTILS_INTERPOSE_19(expr1, expr2) UTILS_INTERPOSE_18(expr1, expr2), expr1 ## 19 expr2
#define UTILS_INTERPOSE_20(expr1, expr2) UTILS_INTERPOSE_19(expr1, expr2), expr1 ## 20 expr2

#define UTILS_INTERPOSE_X_TIMES(x, expr1, expr2) UTILS_INTERPOSE_ ## x(expr1, expr2)
#define UTILS_GENERATE_X_TIMES(x, expr) UTILS_GENERATE_ ## x(expr)
#define UTILS_GENERATE_DOUBLE_X_TIMES(x, expr1, expr2) UTILS_GENERATE_DOUBLE_ ## x(expr1, expr2)

#endif // HELPERS_HH_
