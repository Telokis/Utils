#include <iostream>
#include "Exceptions.hh"

Utils::LogicException::LogicException(const std::string &msg)
  : logic_error(msg), _msg(msg)
{
}

const char	*Utils::LogicException::what() const throw()
{
  return (this->_msg.c_str());
}

Utils::RuntimeException::RuntimeException(const std::string &msg)
  : runtime_error(msg), _msg(msg)
{
}

const char	*Utils::RuntimeException::what() const throw()
{
  return (this->_msg.c_str());
}
