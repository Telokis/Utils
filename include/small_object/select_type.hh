/**
*   @file   select_type.hh
*   @brief  Describes the select_type class.
*/

#ifndef UTILS_TYPE_IF_HH_
#define UTILS_TYPE_IF_HH_

namespace Utils
{
  /**
  *	  @struct	select_type	select_type.hh	<small_object.hh>
  *
  *	  @tparam	BOOL		The test result.
  *	  @tparam	T1		    The type hold if @a BOOL == true.
  *	  @tparam	T2		    The type hold if @a BOOL == false.
  *
  *	  @brief	Allows to choose between two types based on @a BOOL.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <bool BOOL, typename T1, typename T2>
  struct    select_type
  {
    typedef T2  type;       /**< T1 is @a BOOL is true. T2 otherwise */
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *	  @brief	Partial specialization for @a BOOL == true
  */
  template <typename T1, typename T2>
  struct    select_type <true, T1, T2>
  {
    typedef T1  type;       /**< T1 is @a BOOL is true. T2 otherwise */
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_TYPE_IF_HH_