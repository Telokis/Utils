/**
*   @file   type_equal.hh
*
*   @brief  Contains definition of class Utils::type_equal.
*/

#ifndef UTILS_TYPE_EQUAL_HH_
#define UTILS_TYPE_EQUAL_HH_

#include "small_object/selector.hh"

namespace Utils
{
  /**
  *	@struct type_equal	type_equal.hh	<small_object.hh>
  *
  *	@tparam	T1	The first type to compare.
  *	@tparam	T2	The second type to compare.
  *
  *	@brief	Small object used to compare two types.
  *
  *	It inherits from Utils::true_type if @a T1 == @a T2.
  * It inherits from Utils::false_type otherwise.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T1, typename T2>
  struct type_equal : false_type
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *	@tparam	T	The first type to compare.
  *
  *	@brief	Specialization for @a T1 == @a T2.
  */
  template <typename T>
  struct type_equal<T, T> : true_type
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_TYPE_EQUAL_HH_