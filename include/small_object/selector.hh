/**
*   @file   selector.hh
*
*   @brief  Contains definition of class Utils::selector.
*/

#ifndef UTILS_SELECTOR_HH_
#define UTILS_SELECTOR_HH_

#include "small_object/static_value.hh"

/**
*	@namespace	Utils
*	@brief		Main namespace containing the entire library.
*/
namespace Utils
{
  /**
  *	    @struct	selector	selector.hh	<small_object.hh>
  *
  *	    @tparam	VALUE		The boolean value that @ref selector represents.
  *
  *	    @brief	Small object used to represent a boolean value.
  *
  *   	It is used to represent a boolean value while meta programming.
  * 	Its purpose is to prevent explicit template parameter invocation.
  */
  template <bool VALUE>
  struct selector : static_value<bool, VALUE>
  {
  };

  typedef selector<true>  true_type;  /**< Represents a "true" */
  typedef selector<false> false_type; /**< Represents a "false" */
}

#endif // UTILS_SELECTOR_HH_