# Utils
Old utilities library I made while learning C++98

This library is left as it was the last time I updated it (December, 10th, 2014).
It was made for my student project during which I wasn't allowed to use C++11.
I tried to mimic some concepts I found interesting while reading the C++11 specification.
This is far from perfect but it was almost my first contact with C++.
