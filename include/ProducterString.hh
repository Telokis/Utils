/**
*	@file   ProducterString.hh
*
*	@brief  Defines an stream handler based on a string.
*/

#ifndef PRODUCTERSTRING_HH_
#define PRODUCTERSTRING_HH_

#include "interfaces/IProducterStream.hh"
#include <string>

namespace	Utils
{
  /**
  *   @class  ProducterString  ProducterString.hh <ProducterString.hh>
  *
  *   @brief  Defines a stream handler reading from a string.
  *
  *   @see        @ref IProducterStream describes a ProducterStream
  *   @see        @ref ProducterFile reads from a file
  *   @see        @ref ProducterCin reads from std::cin
  *   @see        #UTILS_PRODUCTER_STREAM_BUFFER_SIZE is an helper tool
  */
  class		ProducterString : public IProducterStream
  {
  public:
    ProducterString(const std::string &stringname);     /**< Constructor taking the std::string to hold */
    ~ProducterString();                                 /**< Destructor */

  public:

    /**
    *   @brief        Function used to get the next available chunk.
    *
    *   @param[out]   ret   A boolean which will be set to false if an error occurred or
    *                       if there were nothing left to read.
    *
    *   @return       The chunk to process. Its size is up to #UTILS_PRODUCTER_STREAM_BUFFER_SIZE
    *                 characters
    */
    std::string	nextString(bool &ret);

    /**
    *   @brief        Function used to test whether the string has been fully read or not.
    *
    *   @return       True if the end of the string has been reached. False otherwise.
    */
    bool	isEOF() const;

  private:
    std::string	_stream;                                /**< std::string representing the stream */
  };

}

#endif // PRODUCTERSTRING_HH_
