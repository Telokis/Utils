#ifndef ITHREAD_HH_
#define ITHREAD_HH_

#include <cstdlib>
#include <string>
#include "functor.hh"
#include "Exceptions.hh"

namespace	Utils
{

  // Class used as first parameter for threads
  class			IThreadFunctor
  {
  public:
    virtual		~IThreadFunctor() {};
    virtual void	operator()(void *arg) = 0;
  }; // IThreadFunctor

  class				IThread : public non_copyable
  {
  public:
    virtual void	join() = 0;
    virtual		~IThread() {};
    virtual void	launch(IThreadFunctor *functor, void *arg = NULL) = 0;
    virtual void	launch(const base_functor<void> &) = 0;
    virtual void	launch(void(*func)(void *), void *arg = NULL) = 0;
    virtual void        terminate() = 0;
  }; // IThread

  namespace	Exceptions
  {
    UTILS_GENERATE_EXCEPTION(ThreadLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(ThreadRuntimeException, RuntimeException);
  }

} // Utils

#endif // ITHREAD_HH_
