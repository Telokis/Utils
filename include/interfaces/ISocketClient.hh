/**
*	@file   ISocketClient.hh
*
*	@brief  Defines an interface for all Socket clients to inherit
*       from.
*/

#ifndef ISOCKETCLIENT_HH_
#define ISOCKETCLIENT_HH_

#include <string>
#include "Exceptions.hh"
#include "non_copyable.hh"

/**
*   @def      UTILS_SOCKET_CLIENT_BUFFER_SIZE
*
*   @brief    Defines the buffer size for all ISocketClient.
*
*   @ingroup  Macros
*/
#define UTILS_SOCKET_CLIENT_BUFFER_SIZE	(4096)

namespace		Utils
{
  class			ISocketClient : public non_copyable
  {
  public:
    virtual		~ISocketClient() {};
    virtual size_t	write(const void *buffer, size_t len) const = 0;
    virtual size_t	read(void *buf, size_t len) = 0;
    virtual bool	connect(const std::string &ip, unsigned short int port) = 0;
  }; // ISocketClient

  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(TCPClientLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(TCPClientRuntimeException, RuntimeException);
  }
} // Utils

#endif // ISOCKETCLIENT_HH_
