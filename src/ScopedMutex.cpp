#include "ScopedMutex.hh"

Utils::ScopedMutex::ScopedMutex(Utils::ILockable *lock)
  : _lock(lock)
{
  _lock->lock();
}

Utils::ScopedMutex::~ScopedMutex()
{
  _lock->unlock();
}