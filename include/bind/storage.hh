#ifndef UTILS_STORAGE_HH_
#define UTILS_STORAGE_HH_

#include <helpers.hh>

namespace Utils
{

#define HELL_GENERATE_STORAGE(current, prev)				\
  template <UTILS_GENERATE_X_TIMES(current, typename A)>		\
  struct	storage ## current					\
    : storage ## prev<UTILS_GENERATE_X_TIMES(prev, A)>			\
    {									\
      storage ## current(UTILS_GENERATE_DOUBLE_X_TIMES(current, A, a))	\
	: storage ## prev<UTILS_GENERATE_X_TIMES(prev, A)>		\
	(UTILS_GENERATE_X_TIMES(prev, a)),				\
	_a ## current(a ## current)					\
	{								\
	};								\
	A ## current	_a ## current;					\
    };									\

  struct	storage0
  {
  };

  template <typename A1>
  struct	storage1 : storage0
  {
    storage1(A1 a1)
      : _a1(a1)
    {
    };
    A1	_a1;
  };

  HELL_GENERATE_STORAGE(2, 1);
  HELL_GENERATE_STORAGE(3, 2);
  HELL_GENERATE_STORAGE(4, 3);
  HELL_GENERATE_STORAGE(5, 4);
  HELL_GENERATE_STORAGE(6, 5);
  HELL_GENERATE_STORAGE(7, 6);
  HELL_GENERATE_STORAGE(8, 7);
  HELL_GENERATE_STORAGE(9, 8);
  HELL_GENERATE_STORAGE(10, 9);
  HELL_GENERATE_STORAGE(11, 10);
  HELL_GENERATE_STORAGE(12, 11);
  HELL_GENERATE_STORAGE(13, 12);
  HELL_GENERATE_STORAGE(14, 13);
  HELL_GENERATE_STORAGE(15, 14);
  HELL_GENERATE_STORAGE(16, 15);
  HELL_GENERATE_STORAGE(17, 16);
  HELL_GENERATE_STORAGE(18, 17);
  HELL_GENERATE_STORAGE(19, 18);
  HELL_GENERATE_STORAGE(20, 19);
}

#endif // UTILS_STORAGE_HH_
