#include "unix/MutexUnix.hh"

Utils::Unix::MutexUnix::MutexUnix()
  : _locked(false)
{
  if (pthread_mutex_init(&this->_mutex, NULL) != 0)
    throw Exceptions::MutexRuntimeException("Could not initialize mutex");
}

Utils::Unix::MutexUnix::~MutexUnix()
{
  if (pthread_mutex_destroy(&this->_mutex) != 0)
	  throw Exceptions::MutexLogicException("Could not destroy mutex");
}

bool	Utils::Unix::MutexUnix::locked() const
{
  return (this->_locked);
}

void	Utils::Unix::MutexUnix::lock()
{
  if (pthread_mutex_lock(&this->_mutex) != 0)
	  throw Exceptions::MutexLogicException("Could not lock mutex");
  this->_locked = true;
}

void	Utils::Unix::MutexUnix::unlock()
{
  this->_locked = false;
  if (pthread_mutex_unlock(&this->_mutex) != 0)
	  throw Exceptions::MutexLogicException("Could not unlock mutex");
}

bool	Utils::Unix::MutexUnix::try_lock()
{
  if (pthread_mutex_trylock(&this->_mutex) == 0)
    {
      this->_locked = true;
      return (true);
    }
  this->_locked = false;
  return (false);
}