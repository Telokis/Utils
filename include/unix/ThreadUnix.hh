#ifndef THREADUNIX_HH_
#define THREADUNIX_HH_

#include <cstdlib>
#include <string>
#include <pthread.h>
#include <signal.h>
#include "interfaces/IThread.hh"

namespace	    Utils
{
	namespace	Unix
	{
		class		    ThreadUnix : public IThread
		{
		public:
			ThreadUnix(IThreadFunctor *functor, void *arg = NULL);
			ThreadUnix(void(*func)(void *), void *arg = NULL);
			ThreadUnix();
			virtual void	join();
			virtual void	launch(IThreadFunctor *functor, void *arg = NULL);
			virtual void	launch(void(*func)(void *), void *arg = NULL);
			virtual void	launch(const base_functor<void> &f);
			virtual void	terminate();
			~ThreadUnix();

		private:
			ThreadUnix(const ThreadUnix &ref);
			ThreadUnix		&operator=(const ThreadUnix &ref);
			pthread_t		_thread;
			bool		_started;

		private:
			struct		        FunctorParams
			{
				FunctorParams(IThreadFunctor *f, void *arg)
					: func(f), param(arg)
				{}
				IThreadFunctor	*func;
				void	        *param;
			}; // FunctorParams

			class		ThreadBindLauncher : public IThreadFunctor
			{
			public:
				ThreadBindLauncher(const base_functor<void> &f);
				~ThreadBindLauncher();
				virtual void	operator()(void *);

			private:
				const base_functor<void> &_f;
			}; // ThreadBindLauncher

			class		    ThreadBasicLauncher : public IThreadFunctor
			{
			public:
				ThreadBasicLauncher(void(*f)(void *));
				~ThreadBasicLauncher();
				virtual void	operator()(void *arg);

			private:
				void(*_f)(void *);
			}; // ThreadBasicLauncher

		private:
			static void		*_handler(void *arg);
		}; // ThreadUnix
	}
} // Utils

#endif // THREADUNIX_HH_
