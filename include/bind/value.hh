#ifndef UTILS_VALUE_HH_
#define UTILS_VALUE_HH_

#include <small_object/instance_of.hh>

namespace Utils
{
  template <typename T>
  struct value : instance_of < T >
  {
    value(const T &obj)
      : _obj(obj)
    {
    }
    T		&get()
    {
      return (_obj);
    }
    const T	&get() const
    {
      return (_obj);
    }
    operator T	&()
    {
      return (_obj);
    }
    operator const T&() const
    {
      return (_obj);
    }

  private:
    T		_obj;
  };
}

#endif // UTILS_VALUE_HH_
