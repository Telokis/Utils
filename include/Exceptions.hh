/**
 *	@file	Exceptions.hh
 *
 *	@brief	Defines user-friendly exceptions and macros to
 *		ease exceptions inheritance.
 */

#ifndef EXCEPTIONS_HH_
#define EXCEPTIONS_HH_

#include <stdexcept>

namespace		        Utils
{
  /**
   *	@interface	IBaseException	Exceptions.hh	<Exceptions.hh>
   *
   *	@brief		Defines a common base for all exceptions in @ref Utils.
   */
  class			        IBaseException : public std::exception
  {
  public:
    virtual             ~IBaseException() throw() {}
    virtual const char	*what() const throw() = 0;		/**< Returns a message describing the error */
  }; // IBaseException

  /**
   *	@class 	LogicException	Exceptions.hh	<Exceptions.hh>
   *
   *	@brief	A base for Logical exceptions.
   *
   *	A logical exception is an exception thrown because of
   *		static errors. It should be fixed by modifying the
   *		source code.
   *
   *	@see @ref UTILS_GENERATE_EXCEPTION(@a exceptionName, @a parent) to auto-generate exceptions
   */
  class			        LogicException : public std::logic_error, virtual public IBaseException
  {
  protected:
    const std::string	_msg;
  public:
    virtual             ~LogicException() throw() {}
    LogicException(const std::string &msg);			/**< Constructors taking a msg describing the error */
    virtual const char	*what() const throw();		/**< Returns a message describing the error */
  }; // LogicException

  /**
  *	@class 	RuntimeException	Exceptions.hh	<Exceptions.hh>
  *
  *	@brief	A base for Runtime exceptions.
  *
  *	A Runtime exception is an exception thrown because of
  *		runtime errors. Modifying the source code is not
  *		expected to fix the error.
  *
  *	@see @ref UTILS_GENERATE_EXCEPTION(@a exceptionName, @a parent) to auto-generate exceptions
  */
  class			        RuntimeException : public std::runtime_error, virtual public IBaseException
  {
  protected:
    const std::string	_msg;
  public:
    virtual             ~RuntimeException() throw() {}
    RuntimeException(const std::string &msg);		/**< Constructors taking a msg describing the error */
    virtual const char	*what() const throw();		/**< Returns a message describing the error */
  }; // RuntimeException

  /**
   *	@def	  UTILS_GENERATE_EXCEPTION(exceptionName, parent)
   *
   *	@param	  exceptionName	The name of the exception to generate.
   *	@param	  parent			The parent which @a exceptionName should inherit from.
   *
   *    @ingroup  Macros
   *
   *	@brief	  Generates an exception.
   *
   *	Generates an basic exception named @a exceptionName inheriting from
   *		@a parent.
   */
#	define UTILS_GENERATE_EXCEPTION(exceptionName, parent) \
	class exceptionName : public parent                 \
    {                                                     \
  public:                                               \
    exceptionName(const std::string &msg)               \
      : parent(std::string(#exceptionName) + msg)       \
  	{                                                   \
  	}                                                   \
    ~exceptionName() throw() {}                         \
    };

} // Utils

#endif // EXCEPTIONS_HH_
