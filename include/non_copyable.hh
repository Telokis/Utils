#ifndef NON_COPYABLE_HH_
#define NON_COPYABLE_HH_

/**
* @file   non_copyable.hh
* @brief  Describes the non_copyable class.
*/

namespace Utils
{
  /**
  * @class  non_copyable  non_copyable.hh <non_copyable.hh>
  *
  * @brief  Represents a class which can't be copied.
  * 
  * To use it, simply inherit from it this way:
  * @code{.cpp}
  * class   test : public Utils::non_copyable
  * {
  *   // This class can't be copied !
  * };
  * @endcode
  */
  class non_copyable
  {
  private:
    non_copyable(const non_copyable&);
    non_copyable &operator=(const non_copyable&);

  protected:
    non_copyable()          /**< Protected constructor to allow child construction */
    {
    }
    ~non_copyable()         /**< Protected destructor to allow child destruction */
    {
    }
  };
}

#endif // NON_COPYABLE_HH_