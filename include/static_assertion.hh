/**
*	@file static_assertion.hh
*
*	@brief Contains a set of tools to make static assertions.
*/

/**
*   @defgroup Macros  Macro list
*
*   @brief  This module groups all preprocessor macros
*       defined or used by the Utils library.
*/

#ifndef UTILS_STATIC_ASSERTION_HH_
#define UTILS_STATIC_ASSERTION_HH_

namespace Utils
{
  /**
  *	@struct static_assertion	static_assertion.hh	<static_assertion.hh>
  *
  *	@tparam	STATEMENT			The boolean value of the assertion.
  *
  *	@brief	Small object used to assert static expressions.
  *
  *	A successful assertion is @a static_assertion<true>.
  *
  *	Here is an usage example :
  *	@code{.cpp}
  *	static_assertion<sizeof(double) != 8> ASSERTION_FAILED_DOUBLE_INVALID;
  *	// The line above won't compile if sizeof(double) != 8.
  *	@endcode
  *
  *	@see #UTILS_ASSERT(@a statement) for easier use.
  */
  template <bool STATEMENT>
  struct static_assertion
  {
  };

  /**
  *	@brief	Specialized @ref static_assertion for @a STATEMENT == false.
  *	Represents a failed assertion which won't compile because
  *	@a static_assertion<false> is an incomplete type.
  */
  template <>
  struct static_assertion < false > ;

  /**
   *	@def	  UTILS_ASSERT(statement)
   *
   *    @ingroup  Macros
   *
   *	@param	  statement	The test result.
   *
   *	@brief	  Used as a shortcut to make static assertions.
   *	Simply use @a UTILS_ASSERT with @a statement test between brackets.
   */
#	define	UTILS_ASSERT(statement)	(sizeof(Utils::static_assertion<(statement)>))

  /**
  *	@def	  UTILS_ASSERT_LEGAL(statement)
  *
  * @ingroup  Macros
  *
  *	@param	  statement	The C++ expression to test (@a true or @a false).
  *
  *	@brief	  Tests whether statement is a valid C++ expression.
  *	Its purpose is to assert things for a given type @a T while metaprogramming.
  *
  *	Given the following example code :
  *	@code{.cpp}
  *	template <typename T>
  *	void do_something(T x)
  *	{
  *		UTILS_ASSERT_LEGAL(static_cast<bool>(x.empty()));
  *
  *		if (x.empty())
  *			// ...
  *	@endcode
  *	This snippet of code won't compile if @a T doesn't provide a member @a empty
  *		returning a boolean or anything convertible to boolean.
  */
#	define	UTILS_ASSERT_LEGAL(statement)	(sizeof((statement)))

  /**
  *	@def	  UTILS_CONST_REF_TO(T)
  *
  * @ingroup  Macros
  *
  *	@param	  T	The type to cast.
  *
  *	@brief	  Represents a <em>const T &</em>
  *
  *	Expected to be used combined with @ref UTILS_ASSERT_LEGAL.
  *
  * @return   A representation of a const reference on @a T.
  *
  *	@see      #UTILS_REF_TO(@a T) for non-const reference on T.
  */
#	define	UTILS_CONST_REF_TO(T)	(*static_cast<const (T)*>(0))

  /**
  *	@def	  UTILS_REF_TO(T)
  *
  * @ingroup  Macros
  *
  *	@param	  T	The type to cast.
  *
  *	@brief	  Represents a <em>T &</em>
  *
  *	Expected to be used combined with @ref UTILS_ASSERT_LEGAL.
  *
  * @return   A representation of a reference on @a T.
  *
  *	@see      #UTILS_CONST_REF_TO(@a T) for const reference on T.
  */
#	define	UTILS_REF_TO(T)	(*static_cast<(T)*>(0))
}

#endif // UTILS_STATIC_ASSERTION_HH_