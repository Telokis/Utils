#ifndef UTILS_BIND_TRAITS_HH_
#define UTILS_BIND_TRAITS_HH_

#include <bind/value.hh>
#include <ref.hh>
#include <small_object/select_type.hh>

namespace Utils
{
  template <typename T>
  struct get_type
    : instance_of < value<T> >
  {
  };

  template <typename T>
  struct get_type <reference_wrapper<T> >
    : instance_of < reference_wrapper<T> >
  {
  };
}

#endif // UTILS_BIND_TRAITS_HH_
