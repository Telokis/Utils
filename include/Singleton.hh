/**
*	@file Singleton.hh
*
*	@brief Contains a Singleton class.
*/

#ifndef SINGLETON_HH_
#define SINGLETON_HH_

#include <cstdlib>
#include "non_copyable.hh"

namespace	    Utils
{
  /**
  *	@struct Singleton	Singleton.hh	<Singleton.hh>
  *
  *	@tparam	T			The type that @ref Singleton has to hold.
  *
  *	@brief	Object which should not be instantiated more than once
  *	in a program.
  *
  *	Uses Curiously recurring template pattern.
  *	Example of application :
  *	@code
  *	#include <Singleton.hh>
  *	#include <iostream>
  *
  *	struct test : public Utils::Singleton<test>
  *	{
  *		UTILS_DECLARE_SINGLETON_MEMBERS(test) // Sets constructor and destructor.
  *
  *	public:
  *		void print(int i)
  *		{
  *			std::cout << "i = " << i << std::endl;
  *		}
  *	};
  *
  *	int main()
  *	{
  *		test::getInstance()->print(128);    // Prints "i = 128"
  *		test::destroyInstance();            // Don't forget to destroy the instance
  *		return (0);
  *	}
  *	@endcode
  *
  *	@see http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
  */
  template <typename T>
  class		    Singleton : public non_copyable
  {
  public:
    /**
     *	@brief Returns the instance of @a T.
     *
     *	Instantiates a new @a T on first call.
     *	Returns internal instance otherwise.
     *
     *  @return The current instance.
     */
    static T *getInstance()
    {
      if (_instance == NULL)
        _instance = new T();
      return (_instance);
    }

    /**
    *	@brief Destroys the current instance of @a T.
    *
    *	Also sets the internal instance to NULL to allow reallocation.
    */
    static void	destroyInstance()
    {
      delete _instance;
      _instance = NULL;
    }

  protected:
    Singleton() {} /**< Protected constructor needed for inheritance */

  private:
    static T	*_instance; /**< Private static member @a T* */
  }; // Singleton

  template <typename T> T *Singleton<T>::_instance; /**< Initializing @ref _instance */

} // Utils

/**
 *	@def	  UTILS_DECLARE_SINGLETON_MEMBERS(className)
 *
 *  @ingroup  Macros
 *
 *	@param	  className	The inheriting class name.
 *
 *	@brief	  Declares members properly.
 *
 *	Due to CRTP and private constructors, @a Singleton<className>
 *		has to be friend with @a className.
 */
#define UTILS_DECLARE_SINGLETON_MEMBERS(className)  \
  friend class Utils::Singleton<className>;         \
  private:                                          \
    className() {}                                  \
    ~className() {}

#endif // SINGLETON_HH_
