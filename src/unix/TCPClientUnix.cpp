#include "unix/TCPClientUnix.hh"

Utils::Unix::TCPClientUnix::TCPClientUnix()
  : _socket(-1)
{
}

Utils::Unix::TCPClientUnix::~TCPClientUnix()
{
  if (_socket != -1)
    close(this->_socket);
}

size_t			Utils::Unix::TCPClientUnix::write(const void *buf, size_t len) const
{
  if (_socket == -1)
	  throw Exceptions::TCPClientLogicException("Can't send because socket is uninitialized");
  return (::write(_socket, buf, len));
}

bool			Utils::Unix::TCPClientUnix::connect(const std::string &ip, unsigned short int port)
{
  struct protoent       *proto;
  struct sockaddr_in    sin;

  proto = getprotobyname("TCP");
  if ((_socket = socket(AF_INET, SOCK_STREAM, proto->p_proto)) < 0)
    return (false);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = inet_addr(ip.c_str());
  if ((::connect(_socket, (const struct sockaddr *)&sin, sizeof(sin))) < 0)
    return (false);
  return (true);
}

size_t			Utils::Unix::TCPClientUnix::read(void *buf, size_t len)
{
  int			read_len;

  if (_socket == -1)
	  throw Exceptions::TCPClientLogicException("Can't receive because socket is uninitialized");
  read_len = ::read(_socket, buf, len);
  return (read_len);
}

void			Utils::Unix::TCPClientUnix::initializeFromAccept(int sock)
{
  if (sock < 0)
	  throw Exceptions::TCPClientRuntimeException("TCP is invalid");
  if (_socket != -1)
    throw Exceptions::TCPClientRuntimeException("Already initialized");
  _socket = sock;
}