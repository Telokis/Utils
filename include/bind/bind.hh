#ifndef UTILS_BIND_HH_
#define UTILS_BIND_HH_

#include <bind/bind_list.hh>
#include <bind/caller.hh>
#include <bind/bind_traits.hh>
#include <function.hh>
#include <static_assertion.hh>
#include <bind/wrap_method.hh>

namespace Utils
{

#define	HELL_GENERATE_GET_TYPE_BIND_1(k)	typename get_type<k##1>::type
#define	HELL_GENERATE_GET_TYPE_BIND_2(k)	HELL_GENERATE_GET_TYPE_BIND_1(k), typename get_type<k##2>::type
#define	HELL_GENERATE_GET_TYPE_BIND_3(k)	HELL_GENERATE_GET_TYPE_BIND_2(k), typename get_type<k##3>::type
#define	HELL_GENERATE_GET_TYPE_BIND_4(k)	HELL_GENERATE_GET_TYPE_BIND_3(k), typename get_type<k##4>::type
#define	HELL_GENERATE_GET_TYPE_BIND_5(k)	HELL_GENERATE_GET_TYPE_BIND_4(k), typename get_type<k##5>::type
#define	HELL_GENERATE_GET_TYPE_BIND_6(k)	HELL_GENERATE_GET_TYPE_BIND_5(k), typename get_type<k##6>::type
#define	HELL_GENERATE_GET_TYPE_BIND_7(k)	HELL_GENERATE_GET_TYPE_BIND_6(k), typename get_type<k##7>::type
#define	HELL_GENERATE_GET_TYPE_BIND_8(k)	HELL_GENERATE_GET_TYPE_BIND_7(k), typename get_type<k##8>::type
#define	HELL_GENERATE_GET_TYPE_BIND_9(k)	HELL_GENERATE_GET_TYPE_BIND_8(k), typename get_type<k##9>::type
#define	HELL_GENERATE_GET_TYPE_BIND_10(k)	HELL_GENERATE_GET_TYPE_BIND_9(k), typename get_type<k##10>::type
#define	HELL_GENERATE_GET_TYPE_BIND_11(k)	HELL_GENERATE_GET_TYPE_BIND_10(k), typename get_type<k##11>::type
#define	HELL_GENERATE_GET_TYPE_BIND_12(k)	HELL_GENERATE_GET_TYPE_BIND_11(k), typename get_type<k##12>::type
#define	HELL_GENERATE_GET_TYPE_BIND_13(k)	HELL_GENERATE_GET_TYPE_BIND_12(k), typename get_type<k##13>::type
#define	HELL_GENERATE_GET_TYPE_BIND_14(k)	HELL_GENERATE_GET_TYPE_BIND_13(k), typename get_type<k##14>::type
#define	HELL_GENERATE_GET_TYPE_BIND_15(k)	HELL_GENERATE_GET_TYPE_BIND_14(k), typename get_type<k##15>::type
#define	HELL_GENERATE_GET_TYPE_BIND_16(k)	HELL_GENERATE_GET_TYPE_BIND_15(k), typename get_type<k##16>::type
#define	HELL_GENERATE_GET_TYPE_BIND_17(k)	HELL_GENERATE_GET_TYPE_BIND_16(k), typename get_type<k##17>::type
#define	HELL_GENERATE_GET_TYPE_BIND_18(k)	HELL_GENERATE_GET_TYPE_BIND_17(k), typename get_type<k##18>::type
#define	HELL_GENERATE_GET_TYPE_BIND_19(k)	HELL_GENERATE_GET_TYPE_BIND_18(k), typename get_type<k##19>::type
#define	HELL_GENERATE_GET_TYPE_BIND_20(k)	HELL_GENERATE_GET_TYPE_BIND_19(k), typename get_type<k##20>::type

#define	HELL_GENERATE_TYPEDEFS_BIND_1(k)	typedef typename get_type<k##1>::type value_type1;
#define	HELL_GENERATE_TYPEDEFS_BIND_2(k)	HELL_GENERATE_TYPEDEFS_BIND_1(k) typedef typename get_type<k##2>::type value_type2;
#define	HELL_GENERATE_TYPEDEFS_BIND_3(k)	HELL_GENERATE_TYPEDEFS_BIND_2(k) typedef typename get_type<k##3>::type value_type3;
#define	HELL_GENERATE_TYPEDEFS_BIND_4(k)	HELL_GENERATE_TYPEDEFS_BIND_3(k) typedef typename get_type<k##4>::type value_type4;
#define	HELL_GENERATE_TYPEDEFS_BIND_5(k)	HELL_GENERATE_TYPEDEFS_BIND_4(k) typedef typename get_type<k##5>::type value_type5;
#define	HELL_GENERATE_TYPEDEFS_BIND_6(k)	HELL_GENERATE_TYPEDEFS_BIND_5(k) typedef typename get_type<k##6>::type value_type6;
#define	HELL_GENERATE_TYPEDEFS_BIND_7(k)	HELL_GENERATE_TYPEDEFS_BIND_6(k) typedef typename get_type<k##7>::type value_type7;
#define	HELL_GENERATE_TYPEDEFS_BIND_8(k)	HELL_GENERATE_TYPEDEFS_BIND_7(k) typedef typename get_type<k##8>::type value_type8;
#define	HELL_GENERATE_TYPEDEFS_BIND_9(k)	HELL_GENERATE_TYPEDEFS_BIND_8(k) typedef typename get_type<k##9>::type value_type9;
#define	HELL_GENERATE_TYPEDEFS_BIND_10(k)	HELL_GENERATE_TYPEDEFS_BIND_9(k) typedef typename get_type<k##10>::type value_type10;
#define	HELL_GENERATE_TYPEDEFS_BIND_11(k)	HELL_GENERATE_TYPEDEFS_BIND_10(k) typedef typename get_type<k##11>::type value_type11;
#define	HELL_GENERATE_TYPEDEFS_BIND_12(k)	HELL_GENERATE_TYPEDEFS_BIND_11(k) typedef typename get_type<k##12>::type value_type12;
#define	HELL_GENERATE_TYPEDEFS_BIND_13(k)	HELL_GENERATE_TYPEDEFS_BIND_12(k) typedef typename get_type<k##13>::type value_type13;
#define	HELL_GENERATE_TYPEDEFS_BIND_14(k)	HELL_GENERATE_TYPEDEFS_BIND_13(k) typedef typename get_type<k##14>::type value_type14;
#define	HELL_GENERATE_TYPEDEFS_BIND_15(k)	HELL_GENERATE_TYPEDEFS_BIND_14(k) typedef typename get_type<k##15>::type value_type15;
#define	HELL_GENERATE_TYPEDEFS_BIND_16(k)	HELL_GENERATE_TYPEDEFS_BIND_15(k) typedef typename get_type<k##16>::type value_type16;
#define	HELL_GENERATE_TYPEDEFS_BIND_17(k)	HELL_GENERATE_TYPEDEFS_BIND_16(k) typedef typename get_type<k##17>::type value_type17;
#define	HELL_GENERATE_TYPEDEFS_BIND_18(k)	HELL_GENERATE_TYPEDEFS_BIND_17(k) typedef typename get_type<k##18>::type value_type18;
#define	HELL_GENERATE_TYPEDEFS_BIND_19(k)	HELL_GENERATE_TYPEDEFS_BIND_18(k) typedef typename get_type<k##19>::type value_type19;
#define	HELL_GENERATE_TYPEDEFS_BIND_20(k)	HELL_GENERATE_TYPEDEFS_BIND_19(k) typedef typename get_type<k##20>::type value_type20;

#define	HELL_GENERATE_BIND_FUNCTION(x)					\
  template <typename R,							\
	    UTILS_GENERATE_X_TIMES(x, typename FUNC_PARAM),		\
	    UTILS_GENERATE_X_TIMES(x, typename BIND_PARAM)>		\
  caller<R, function<R(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM))>,	\
	 bind_list##x<HELL_GENERATE_GET_TYPE_BIND_##x(BIND_PARAM)> >	\
  bind (R(*f)(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM)),			\
	UTILS_GENERATE_DOUBLE_X_TIMES(x, BIND_PARAM, bind_param))	\
  {									\
    HELL_GENERATE_TYPEDEFS_BIND_##x(BIND_PARAM);			\
      typedef bind_list##x<UTILS_GENERATE_X_TIMES(x, value_type)> list_type; \
	list_type l(UTILS_GENERATE_X_TIMES(x, bind_param));		\
	return (caller<R,						\
		function<R(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM))>, \
		list_type>(f, l));					\
  }

  template <typename R>
  caller<R, function<R()>, bind_list0>
    bind(R(*f)())
  {
    bind_list0 l;
    return (caller<R, function<R()>, bind_list0>(f, l));
  }

  HELL_GENERATE_BIND_FUNCTION(1);
  HELL_GENERATE_BIND_FUNCTION(2);
  HELL_GENERATE_BIND_FUNCTION(3);
  HELL_GENERATE_BIND_FUNCTION(4);
  HELL_GENERATE_BIND_FUNCTION(5);
  HELL_GENERATE_BIND_FUNCTION(6);
  HELL_GENERATE_BIND_FUNCTION(7);
  HELL_GENERATE_BIND_FUNCTION(8);
  HELL_GENERATE_BIND_FUNCTION(9);
  HELL_GENERATE_BIND_FUNCTION(10);
  HELL_GENERATE_BIND_FUNCTION(11);
  HELL_GENERATE_BIND_FUNCTION(12);
  HELL_GENERATE_BIND_FUNCTION(13);
  HELL_GENERATE_BIND_FUNCTION(14);
  HELL_GENERATE_BIND_FUNCTION(15);
  HELL_GENERATE_BIND_FUNCTION(16);
  HELL_GENERATE_BIND_FUNCTION(17);
  HELL_GENERATE_BIND_FUNCTION(18);
  HELL_GENERATE_BIND_FUNCTION(19);
  HELL_GENERATE_BIND_FUNCTION(20);

  // Overloads for a pointer to method
#define	HELL_GENERATE_BIND_METHOD_FUNCTION(x, mod)			\
  template <typename R, typename O,					\
	    UTILS_GENERATE_X_TIMES(x, typename FUNC_PARAM),		\
	    UTILS_GENERATE_X_TIMES(x, typename BIND_PARAM)>		\
  caller<R, function<R(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM))>,	\
	 bind_list##x<HELL_GENERATE_GET_TYPE_BIND_##x(BIND_PARAM)> >	\
  bind (R(O::*f)(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM)) mod, O *o,	\
	UTILS_GENERATE_DOUBLE_X_TIMES(x, BIND_PARAM, bind_param))	\
      {									\
    HELL_GENERATE_TYPEDEFS_BIND_##x(BIND_PARAM);			\
      typedef R(O::*method_type)(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM)) mod; \
      typedef bind_list##x<UTILS_GENERATE_X_TIMES(x, value_type)> list_type; \
	list_type l(UTILS_GENERATE_X_TIMES(x, bind_param));		\
	return (caller<R,						\
		function<R(UTILS_GENERATE_X_TIMES(x, FUNC_PARAM))>, \
		list_type>(method_wrapper<O, method_type>(o, f), l));	\
      }

  template <typename R, typename O>
  caller<R, function<R()>, bind_list0>
    bind(R(O::*f)(), O *o)
  {
    bind_list0 l;
    return (caller<R, function<R()>, bind_list0>(wrap_method(o, f), l));
  }

  HELL_GENERATE_BIND_METHOD_FUNCTION(1, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(2, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(3, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(4, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(5, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(6, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(7, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(8, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(9, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(10, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(11, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(12, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(13, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(14, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(15, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(16, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(17, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(18, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(19, );
  HELL_GENERATE_BIND_METHOD_FUNCTION(20, );

  // Const pointer to method
  template <typename R, typename O>
  caller<R, function<R()>, bind_list0>
    bind(R(O::*f)() const, O *o)
  {
    bind_list0 l;
    return (caller<R, function<R()>, bind_list0>(wrap_method(o, f), l));
  }

  HELL_GENERATE_BIND_METHOD_FUNCTION(1, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(2, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(3, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(4, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(5, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(6, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(7, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(8, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(9, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(10, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(11, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(12, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(13, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(14, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(15, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(16, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(17, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(18, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(19, const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(20, const);

  // Volatile pointer to method
  template <typename R, typename O>
  caller<R, function<R()>, bind_list0>
    bind(R(O::*f)() volatile, O *o)
  {
    bind_list0 l;
    return (caller<R, function<R()>, bind_list0>(wrap_method(o, f), l));
  }

  HELL_GENERATE_BIND_METHOD_FUNCTION(1, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(2, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(3, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(4, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(5, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(6, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(7, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(8, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(9, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(10, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(11, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(12, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(13, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(14, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(15, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(16, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(17, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(18, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(19, volatile);
  HELL_GENERATE_BIND_METHOD_FUNCTION(20, volatile);

  // Volatile const pointer to method
  template <typename R, typename O>
  caller<R, function<R()>, bind_list0>
    bind(R(O::*f)() volatile const, O *o)
  {
    bind_list0 l;
    return (caller<R, function<R()>, bind_list0>(wrap_method(o, f), l));
  }

  HELL_GENERATE_BIND_METHOD_FUNCTION(1, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(2, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(3, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(4, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(5, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(6, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(7, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(8, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(9, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(10, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(11, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(12, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(13, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(14, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(15, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(16, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(17, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(18, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(19, volatile const);
  HELL_GENERATE_BIND_METHOD_FUNCTION(20, volatile const);

}

#endif // UTILS_BIND_HH_
