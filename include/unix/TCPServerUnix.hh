#ifndef TCPSERVERUNIX_HH_
#define TCPSERVERUNIX_HH_

#include <string>
#include "Exceptions.hh"
#include "interfaces/ISocketServer.hh"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE		(4096)

namespace			Utils
{
	namespace	Unix
	{
		class				TCPServerUnix : public ISocketServer
		{
			friend class		TCPClientUnix;
		public:
			TCPServerUnix();
			~TCPServerUnix();

		private:
			TCPServerUnix(const TCPServerUnix &);
			TCPServerUnix		&operator=(const TCPServerUnix &);

		public:
			virtual bool		start(unsigned short int port);
			virtual ISocketClient	*accept() const;

		private:
			int				_socket;
		}; // TCPServerUnix
	}
} // Utils

#endif // TCPSERVERUNIX_HH_
