/**
 *	@file	unique_ptr.hh
 *	@brief	Describes the unique_ptr class.
 */

#ifndef UNIQUE_PTR_HH_
#define UNIQUE_PTR_HH_

#include <cstdlib>
#include <iostream>
#include "non_copyable.hh"

namespace	Utils
{
  /**
  *	@class	unique_ptr	unique_ptr.hh	<unique_ptr.hh>
  *	@brief	Smart pointer holding a <em>T*</em>.
  *
  *	The unique_ptr is responsible for releasing memory on destruction.
  *	unique_ptr is designed to be used as transparently as a nude-pointer.
  *	To do so, it defines a lot of common operators.
  *
  *	@tparam	T	Type to hold. <em>T*</em> is internally stored.
  *
  *	@see	@ref SharedPtr if ownership should be shared.
  */
  template <typename T>
  class		unique_ptr : non_copyable
  {
  public:
    typedef	T	ptr_type;										/**< Typedef on @a T for easier usage. */
    typedef	void(*delete_func)(T *);							/**< Typedef on @a T's expected destructor. */

    /**
     *	@brief	Default constructor.
     *
     *	Initializes the internal pointer to @a ptr and set the deleter to @a f.
     *	Defaults to NULL if no parameters are passed.
     *
     *	@param	ptr	The pointer to hold.
     *	@param	f	The deleter to use.
     */
    unique_ptr(ptr_type *ptr = NULL, delete_func f = NULL)
      : _ptr(ptr), _d(f)
    {
    }

    /**
    *	@brief	Destructor.
    *
    *	Destroys the instance, deleting the internal pointer.
    *	Uses @a delete_func if provided and delete operator otherwise.
    */
    ~unique_ptr()
    {
      if (this->_d == NULL)
        delete (this->_ptr);
      else
        this->_d(this->_ptr);
    }

    /**
    *	@brief	Assignment operator from a nude pointer of type @a ptr_type.
    *
    *	Initializes the internal pointer to @a ptr.
    *	Delete the internal pointer if set.
    *
    *	@param	ptr	The nude ptr to hold.
    *
    *   @return The current @ref SharedPtr instance to allow chaining operations.
    */
    unique_ptr &operator=(ptr_type *ptr)
    {
      if (_ptr != NULL)
      {
        if (this->_d == NULL)
          delete (this->_ptr);
        else
          this->_d(this->_ptr);
      }
      _ptr = ptr;
      return (*this);
    }

    /**
    *	@brief	Overloads the << operator for a std::ostream.
    *
    *	Allows the unique_ptr to be written on a stream as if the
    *		internal pointer was.
    *
    *   @return The stream passed as parameter.
    */
    std::ostream  &operator<<(const std::ostream &os)
    {
      os << this->_ptr;
      return (os);
    }

    /**
    *	@brief	Conversion operator allowing unique_ptr<T> to be treated as a
    *		simple T*.
    *
    *   @return The internal pointer.
    */
    operator T*()
    {
      return (this->get());
    }

    /**
    *	@brief	Addressof operator.
    *
    *   @return The addressof internal pointer.
    */
    T			  **operator&()
    {
      return (&this->_ptr);
    }

    /**
    *	@brief	Registers a new deleter.
    *
    *	@param	f	The @ref delete_func to register.
    */
    void	      registerDestroyFunc(delete_func f)
    {
      this->_d = f;
    }

    /**
    *	@brief	Indirection operator.
    *
    *   @return The pointer to indirect.
    */
    ptr_type	  &operator*() const
    {
      return (*this->_ptr);
    }

    /**
    *	@brief	Equality operator taking a nude pointer.
    *
    *	@param	ptr	The comparing nude pointer.
    *
    *   @return True if the internal ptr is equal to @a ptr. False otherwise.
    */
    bool	      operator==(void *ptr) const
    {
      return (this->_ptr == ptr);
    }

    /**
    *	@brief	Equality operator taking a @ref unique_ptr templated on a different type.
    *
    *	@param	ptr	The comparing @ref unique_ptr.
    *
    *   @return True if the internal ptr is equal to @a ptr's one. False otherwise.
    */
    template <typename U>
    bool	      operator==(const unique_ptr<U> &ptr) const
    {
      return (this->_ptr == ptr.get());
    }

    /**
    *	@brief	NotEqual operator taking a nude pointer.
    *
    *	@param	ptr	The comparing nude pointer.
    *
    *   @return True if the internal ptr is different from @a ptr. False otherwise.
    */
    bool	      operator!=(void *ptr) const
    {
      return (this->_ptr != ptr);
    }

    /**
    *	@brief	NotEqual operator taking a @ref SharedPtr templated on a different type.
    *
    *	@param	ptr	The comparing @ref SharedPtr.
    *
    *   @return True if the internal ptr is different from @a ptr's one. False otherwise.
    */
    template <typename U>
    bool	      operator!=(const unique_ptr<U> &ptr) const
    {
      return (this->_ptr != ptr.get());
    }

    /**
    *	@brief	Member of pointer operator.
    *
    *   @return The internal pointer to access.
    */
    ptr_type	  *operator->() const
    {
      return (this->_ptr);
    }

    /**
    *	@brief	Resets a unique_ptr to NULL.
    *
    *	Releases memory hold by internal pointer.
    *	Comparison with NULL is now true.
    */
    void	      reset()
    {
      if (this->_d == NULL)
        delete (this->_ptr);
      else
        this->_d(this->_ptr);
      this->_ptr = NULL;
    }

    /**
    *	@brief	Nude pointer accessor.
    *
    *   @return The internal pointer.
    */
    ptr_type	  *get() const
    {
      return (this->_ptr);
    }

    /**
    *	@brief	Deleter accessor.
    *
    *   @return The deleter function. NULL if not set.
    */
    delete_func	  getDeletor() const
    {
      return (this->_d);
    }

  private:
    ptr_type	  *_ptr;
    delete_func	  _d;
  }; // unique_ptr

};

#endif // UNIQUE_PTR_HH_
