#ifndef META_ZERO_OR_ONE_HH_
#define META_ZERO_OR_ONE_HH_

namespace Utils
{

  // +-------------------------+
  // | For a pointer to method |
  // +-------------------------+

  // For zero parameter
  template <class P>
  inline bool	zero_or_one(bool (P::*func)(), P &parser)
  {
    (parser.*func)();
    return (true);
  }

  // For zero parameter
  template <class P, class A0>
  inline bool	zero_or_one(bool (P::*func)(A0), P &parser, A0 arg0)
  {
    (parser.*func)(arg0);
    return (true);
  }

  // For two parameters
  template <class P, class A0, class A1>
  inline bool	zero_or_one(bool (P::*func)(A0, A1), P &parser, A0 arg0, A1 arg1)
  {
    (parser.*func)(arg0, arg1);
    return (true);
  }

  // +---------------------------+
  // | For a pointer to function |
  // +---------------------------+

  // For zero parameter
  inline bool	zero_or_one(bool (*func)())
  {
    func();
    return (true);
  }

  // For zero parameter
  template <class A0>
  inline bool	zero_or_one(bool (*func)(A0), A0 arg0)
  {
    func(arg0);
    return (true);
  }

  // For two parameters
  template <class A0, class A1>
  inline bool	zero_or_one(bool (*func)(A0, A1), A0 arg0, A1 arg1)
  {
    func(arg0, arg1);
    return (true);
  }

} // Utils

#endif // META_ZERO_OR_ONE_HH_
