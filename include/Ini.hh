#ifndef INI_HH_
#define INI_HH_

#include <string>
#include <fstream>
#include <map>
#include "Exceptions.hh"
#include <list>
#include "Mutex.hh"

namespace		Utils
{
  class			Ini : public non_copyable
  {
  private:
    typedef std::map<std::string, std::string>		t_ini_sub_map;
    typedef std::map<std::string, t_ini_sub_map >	t_ini_map;

    struct		_Positions
    {
      _Positions();
      _Positions(const std::string &line);
      void		process(const std::string &line);
      size_t		_firstChar;
      size_t		_lastChar;
      size_t		_comment;
      size_t		_beginSection;
      size_t		_endSection;
      size_t		_equal;
      size_t		_firstAfterEqual;
      size_t		_lastEqual;
      size_t		_lastBeginSection;
      size_t		_lastEndSection;
      size_t		_firstQuote;
      size_t		_lastQuote;
    }; // _Positions

  public:
    Ini();
    Ini(const std::string &filename);
    ~Ini();
    void		          parse();
    void		          open(std::string const &filename, std::ios_base::openmode mode = std::fstream::in | std::fstream::out);
    void		          open(std::ios_base::openmode mode = std::fstream::in | std::fstream::out);
    void		          close();
    std::string		      get(const std::string &section, const std::string &key, const std::string &def) const;
    const t_ini_sub_map	  &getSection(const std::string &section) const;
    void		          put(const std::string &section, const std::string &key, const std::string &value);
    void		          save();
    void		          errors(int num, const Utils::Ini::_Positions &pos);

  private:
    bool		  _opened;
    std::string	  _filename;
    std::fstream  _stream;
    t_ini_map	  _map;
    IMutex		  *_mutex;
  }; // Ini

  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(IniLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(IniRuntimeException, RuntimeException);
  } // Exceptions
} // Utils

#endif // INI_HH_
