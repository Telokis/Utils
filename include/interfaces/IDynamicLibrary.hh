#ifndef IDYNAMICLIBRARY_HH_
#define IDYNAMICLIBRARY_HH_

#include "Exceptions.hh"
#include "non_copyable.hh"
#include <string>

namespace				Utils
{
  class					IDynamicLibrary : public non_copyable
  {
  public:
    virtual				~IDynamicLibrary() {};
    virtual void		*findFunc(std::string const &name) = 0;
    virtual bool		load(std::string const &path) = 0;
    virtual void		unload() = 0;
  }; // IDynamicLibrary

  namespace Exceptions
  {
    UTILS_GENERATE_EXCEPTION(DynamicLibraryLogicException, LogicException);
    UTILS_GENERATE_EXCEPTION(DynamicLibraryRuntimeException, RuntimeException);
  }
} // Utils

#endif // IDYNAMICLIBRARY_HH_
