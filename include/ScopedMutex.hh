#ifndef SCOPEDMUTEX_HH_
#define SCOPEDMUTEX_HH_

#include "interfaces/ILockable.hh"

namespace		  Utils
{
  class			  ScopedMutex
  {
  public:
    ScopedMutex(Utils::ILockable *lock);
    ~ScopedMutex();

  private:
    ScopedMutex(ScopedMutex &_copy);
    ScopedMutex	  &operator=(ScopedMutex &_copy);
    ILockable	  *_lock;
  }; // ScopedMutex

} // Utils

#endif // SCOPEDMUTEX_HH_