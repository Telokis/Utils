#ifndef VECTOR3_HH_
#define VECTOR3_HH_

#include <iostream>

namespace	    Utils
{

  template <typename T>
  class		    Vector3
  {
  public:
    Vector3();
    Vector3(T _x, T _y, T _z);
    Vector3(T a);
    virtual	    ~Vector3() {}
    Vector3(const Vector3 &ref);
    Vector3	    &operator=(const Vector3 &ref);

  public:
    // Add
    Vector3	    &operator+=(const Vector3 &ref);
    Vector3	    &operator+=(const T &n);
    Vector3	    operator+(const Vector3<T> &b) const;
    Vector3	    operator+(const T &b) const;

    // Sub
    Vector3	    &operator-=(const Vector3 &ref);
    Vector3	    &operator-=(const T &n);
    Vector3	    operator-(const Vector3<T> &b) const;
    Vector3	    operator-(const T &b) const;

    // Mult
    Vector3	    &operator*=(const Vector3 &ref);
    Vector3	    &operator*=(const T &n);
    Vector3	    operator*(const Vector3<T> &b) const;
    Vector3	    operator*(const T &b) const;

    // Div
    Vector3	    &operator/=(const Vector3 &ref);
    Vector3	    &operator/=(const T &n);
    Vector3	    operator/(const Vector3<T> &b) const;
    Vector3	    operator/(const T &b) const;

    // Compare
    bool	    operator==(const Vector3 &ref) const;
    bool	    operator!=(const Vector3 &ref) const;

  public:
    void	    normalize();
    void	    serialize(std::ostream &os) const;

  public:
    static Vector3	normalize(const Vector3 &v);

  public:
    T	        x;
    T	        y;
    T	        z;
  }; // Vector3

  template <typename T>
  std::ostream	&operator<<(std::ostream &str, const Vector3<T> &v)
  {
    v.serialize(str);
    return (str);
  }

  // Useful typedefs for easier use
  typedef	Vector3<float>          	vec3;
  typedef	Vector3<float>	            fvec3;
  typedef	Vector3<int>	            ivec3;
  typedef	Vector3<double>	            dvec3;
  typedef	Vector3<char>	            cvec3;
  typedef	Vector3<unsigned int>		uivec3;
  typedef	Vector3<unsigned char>		ucvec3;
} // Utils

#endif // VECTOR3_HH_
