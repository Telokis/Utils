#ifndef IBASICSOCKETSERVER_HH_
#define IBASICSOCKETSERVER_HH_

#include "non_copyable.hh"

namespace		Utils
{

  class			IBasicSocketServer : public non_copyable
  {
  public:
    virtual		~IBasicSocketServer() {};
    virtual bool	start(unsigned short int port) = 0;
  }; // IBasicSocketServer

} // Utils

#endif // IBASICSOCKETSERVER_HH_
