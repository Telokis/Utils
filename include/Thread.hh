#ifndef THREAD_HH_
#define THREAD_HH_

#include "interfaces/IThread.hh"

#if defined(_WIN32) || defined(WIN32) // WINDOWS
#include "windows/ThreadWindows.hh"

namespace Utils
{
	inline IThread	*newThread()
	{
		return (new Windows::ThreadWindows);
	}
}

#elif defined(__unix) // UNIX
#include "unix/ThreadUnix.hh"

namespace Utils
{
	inline IThread	*newThread()
	{
		return (new Unix::ThreadUnix);
	}
}
#else
#error "Unknown platform"
#endif // Platform recognition

#endif // THREAD_HH_
