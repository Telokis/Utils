#ifndef FUNCTION_HPP_
#define FUNCTION_HPP_

#include <iostream>
#include <SharedPtr.hh>
#include <helpers.hh>

namespace Utils
{

#define UTILS_FUNCTION_MEMBERS				\
							\
  function()						\
  {							\
  }							\
  /* Destructor*/					\
  ~function()						\
  {							\
  }							\
							\
  /* Copy constructor*/					\
  function(const function &f)				\
  {							\
    wrapper = f.wrapper;				\
  }							\
							\
  /* Assignment operator*/				\
  function &operator=(const function &f)		\
  {							\
    wrapper.reset();					\
    wrapper = f.wrapper;				\
    return (*this);					\
  }							\
							\
  /* Copy constructor from a callable*/			\
  template <typename U>					\
  function(U callable)					\
    : wrapper(new callable_wrapper<U>(callable))	\
  {							\
  }							\
							\
  /* Assignment operator from a callable*/		\
  template <typename U>					\
  function  &operator=(U callable)			\
  {							\
    wrapper.reset();					\
    wrapper = new callable_wrapper<U>(callable);	\
    return (*this);					\
  }							\
							\
  /* Copy constructor from a pointer to function*/	\
  function(func_type func)				\
    : wrapper(new func_wrapper(func))			\
  {							\
  }							\
							\
  /* Assignment operator from a pointer to function*/	\
  function  &operator=(func_type func)			\
  {							\
    wrapper.reset();					\
    wrapper = new func_wrapper(func);			\
    return (*this);					\
  }

  // Begin generation
#define UTILS_GENERATE_FUNCTION(x)				\
  template <typename R, UTILS_GENERATE_X_TIMES(x, typename A)>	\
  struct function < R(UTILS_GENERATE_X_TIMES(x, A)) >		\
  {								\
private:							\
 typedef R(*func_type)(UTILS_GENERATE_X_TIMES(x, A));		\
								\
 struct wrapper_base						\
 {								\
  virtual ~wrapper_base() {}					\
  virtual R operator()(UTILS_GENERATE_X_TIMES(x, A)) = 0;	\
};								\
								\
 template <typename T>						\
 struct callable_wrapper : wrapper_base				\
 {								\
  callable_wrapper(T t)						\
    : _f(t)							\
  {								\
}								\
  ~callable_wrapper() {}					\
  R operator()(UTILS_GENERATE_DOUBLE_X_TIMES(x, A, a))		\
  {								\
  return (_f(UTILS_GENERATE_X_TIMES(x, a)));			\
}								\
private:							\
 T _f;								\
};								\
								\
 struct func_wrapper : wrapper_base				\
 {								\
  func_wrapper(func_type f)					\
    : _f(f)							\
  {								\
}								\
  ~func_wrapper() {}						\
  R operator()(UTILS_GENERATE_DOUBLE_X_TIMES(x, A, a))		\
  {								\
  return (_f(UTILS_GENERATE_X_TIMES(x, a)));			\
}								\
private:							\
 func_type _f;							\
};								\
								\
 SharedPtr<wrapper_base> wrapper;				\
								\
public:								\
 UTILS_FUNCTION_MEMBERS;					\
								\
 R         operator()(UTILS_GENERATE_DOUBLE_X_TIMES(x, A, a))	\
 {								\
  return (wrapper->operator()(UTILS_GENERATE_X_TIMES(x, a)));	\
}								\
};
  // END GENERATION

  template <typename F>
  struct function;

  // function for no params
  template <typename R>
  struct function < R() >
  {
  private:
    // Pointer to function type
    typedef R(*func_type)();

    // Base class for wrappers
    struct wrapper_base
    {
      virtual ~wrapper_base() {}
      virtual R operator()() = 0;
    };

    // Wraps a callable function
    template <typename T>
    struct callable_wrapper : wrapper_base
    {
      callable_wrapper(T t)
	: _f(t)
      {
      }
      ~callable_wrapper() {}
      R operator()()
      {
	return (_f());
      }
    private:
      T _f;
    }; // func_wrapper

    // Wraps a pointer to function
    struct func_wrapper : wrapper_base
    {
      func_wrapper(func_type f)
	: _f(f)
      {
      }
      ~func_wrapper() {}
      R operator()()
      {
	return (_f());
      }
    private:
      func_type _f;
    }; // func_wrapper

    // SharedPtr to share wrappers
    SharedPtr<wrapper_base> wrapper;

  public:
    UTILS_FUNCTION_MEMBERS;

    // Caller
    R         operator()()
    {
      return (wrapper->operator()());
    }
  };

  UTILS_GENERATE_FUNCTION(1);
  UTILS_GENERATE_FUNCTION(2);
  UTILS_GENERATE_FUNCTION(3);
  UTILS_GENERATE_FUNCTION(4);
  UTILS_GENERATE_FUNCTION(5);
  UTILS_GENERATE_FUNCTION(6);
  UTILS_GENERATE_FUNCTION(7);
  UTILS_GENERATE_FUNCTION(8);
  UTILS_GENERATE_FUNCTION(9);
  UTILS_GENERATE_FUNCTION(10);
  UTILS_GENERATE_FUNCTION(11);
  UTILS_GENERATE_FUNCTION(12);
  UTILS_GENERATE_FUNCTION(13);
  UTILS_GENERATE_FUNCTION(14);
  UTILS_GENERATE_FUNCTION(15);
  UTILS_GENERATE_FUNCTION(16);
  UTILS_GENERATE_FUNCTION(17);
  UTILS_GENERATE_FUNCTION(18);
  UTILS_GENERATE_FUNCTION(19);
  UTILS_GENERATE_FUNCTION(20);
} // Utils

#endif
