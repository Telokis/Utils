#include "windows/TCPServerWindows.hh"
#include "windows/TCPClientWindows.hh"

Utils::Windows::TCPServerWindows::TCPServerWindows()
  : _socket(INVALID_SOCKET)
{
  WSADATA		wsaData;

  if ((WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)
	  throw Exceptions::TCPServerRuntimeException("Unable to startup properly");
}

Utils::Windows::TCPServerWindows::~TCPServerWindows()
{
  if (this->_socket != INVALID_SOCKET)
  {
    if ((closesocket(_socket)) == SOCKET_ERROR)
		throw Exceptions::TCPServerRuntimeException("Error while closing socket");
  }
  WSACleanup();
}

Utils::ISocketClient	*Utils::Windows::TCPServerWindows::accept() const
{
  SOCKET			clientTCP = INVALID_SOCKET;
  sockaddr_in		sinClient;
  int				len = sizeof(sinClient);

  if (_socket == INVALID_SOCKET)
	  throw Exceptions::TCPServerLogicException("Server isn't initialized. Call start first.");
  if ((clientTCP = WSAAccept(_socket, (SOCKADDR*)&sinClient, &len, NULL, NULL)) == 0)
	  throw Exceptions::TCPServerRuntimeException("Unable to accept client");
  ISocketClient		*client = new TCPClientWindows;
  ((TCPClientWindows*)client)->initializeFromAccept(clientTCP);
  return (client);
}

bool				Utils::Windows::TCPServerWindows::start(unsigned short int port)
{
  sockaddr_in		sin;

  if ((this->_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0)) == INVALID_SOCKET)
    return (false);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = INADDR_ANY;
  if (bind(this->_socket, (const struct sockaddr *)&sin, sizeof(sin)) == SOCKET_ERROR)
    return (false);
  if (listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
    return (false);
  return (true);
}