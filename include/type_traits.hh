/**
*   @file   type_traits.hh
*   @brief  Shortcut to include all type_traits.
*/

#ifndef UTILS_TYPE_TRAITS_HH_
#define UTILS_TYPE_TRAITS_HH_

#include "type_traits/add_const.hh"
#include "type_traits/add_pointer.hh"
#include "type_traits/add_reference.hh"
#include "type_traits/function_traits.hh"
#include "type_traits/is_const.hh"
#include "type_traits/is_function.hh"
#include "type_traits/is_pointer.hh"
#include "type_traits/is_pointer_to_function.hh"
#include "type_traits/is_pointer_to_method.hh"
#include "type_traits/is_scalar.hh"
#include "type_traits/optimal_parameter.hh"
#include "type_traits/remove_const.hh"
#include "type_traits/remove_pointer.hh"
#include "type_traits/remove_reference.hh"

#endif // UTILS_TYPE_TRAITS_HH_