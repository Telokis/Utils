/**
*   @file   remove_reference.hh
*   @brief  Describes Utils::remove_reference.
*/

#ifndef UTILS_REMOVE_REFERENCE_HH_
#define UTILS_REMOVE_REFERENCE_HH_

#include <small_object/instance_of.hh>

namespace Utils
{
  /**
  *   @struct   remove_reference  remove_reference.hh   <type_traits.hh>
  *   @brief    removes a reference qualifier from @a T.
  *
  *   remove_reference<T &>::type == T.
  *
  *   @tparam   T The type to remove referenceness from.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  remove_reference : instance_of<T>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for T &.
  */
  template <typename T>
  struct  remove_reference <T &> : instance_of<T>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_REMOVE_REFERENCE_HH_
