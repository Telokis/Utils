#ifndef UTILS_WRAP_METHOD_HH_
#define UTILS_WRAP_METHOD_HH_

#include <bind/method_wrapper.hh>

namespace Utils
{
#define HELL_GENERATE_POINTER_WRAPPER(x, mod)				\
  template <typename O, typename R, UTILS_GENERATE_X_TIMES(x, typename A)> \
  method_wrapper<O, R(O::*)(UTILS_GENERATE_X_TIMES(x, A)) mod>		\
  wrap_method(O *obj, R(O::*f)(UTILS_GENERATE_X_TIMES(x, A)))		\
  {									\
    typedef method_wrapper<O, R(O::*)(UTILS_GENERATE_X_TIMES(x, A)) mod> return_type; \
    return (return_type(obj, f));					\
  }

  template <typename O, typename R>
  method_wrapper<O, R(O::*)()>	wrap_method(O *obj, R(O::*f)())
  {
    typedef method_wrapper<O, R(O::*)()> return_type;
    return (return_type(obj, f));
  }

  template <typename O, typename R>
  method_wrapper<O, R(O::*)() const>	wrap_method(O *obj, R(O::*f)() const)
  {
    typedef method_wrapper<O, R(O::*)() const> return_type;
    return (return_type(obj, f));
  }

  template <typename O, typename R>
  method_wrapper<O, R(O::*)() volatile>	wrap_method(O *obj, R(O::*f)() volatile)
  {
    typedef method_wrapper<O, R(O::*)() volatile> return_type;
    return (return_type(obj, f));
  }

  template <typename O, typename R>
  method_wrapper<O, R(O::*)() const volatile>	wrap_method(O *obj, R(O::*f)() const volatile)
  {
    typedef method_wrapper<O, R(O::*)() const volatile> return_type;
    return (return_type(obj, f));
  }

  HELL_GENERATE_POINTER_WRAPPER(1, );
  HELL_GENERATE_POINTER_WRAPPER(2, );
  HELL_GENERATE_POINTER_WRAPPER(3, );
  HELL_GENERATE_POINTER_WRAPPER(4, );
  HELL_GENERATE_POINTER_WRAPPER(5, );
  HELL_GENERATE_POINTER_WRAPPER(6, );
  HELL_GENERATE_POINTER_WRAPPER(7, );
  HELL_GENERATE_POINTER_WRAPPER(8, );
  HELL_GENERATE_POINTER_WRAPPER(9, );
  HELL_GENERATE_POINTER_WRAPPER(10, );
  HELL_GENERATE_POINTER_WRAPPER(11, );
  HELL_GENERATE_POINTER_WRAPPER(12, );
  HELL_GENERATE_POINTER_WRAPPER(13, );
  HELL_GENERATE_POINTER_WRAPPER(14, );
  HELL_GENERATE_POINTER_WRAPPER(15, );
  HELL_GENERATE_POINTER_WRAPPER(16, );
  HELL_GENERATE_POINTER_WRAPPER(17, );
  HELL_GENERATE_POINTER_WRAPPER(18, );
  HELL_GENERATE_POINTER_WRAPPER(19, );
  HELL_GENERATE_POINTER_WRAPPER(20, );

  HELL_GENERATE_POINTER_WRAPPER(1, const);
  HELL_GENERATE_POINTER_WRAPPER(2, const);
  HELL_GENERATE_POINTER_WRAPPER(3, const);
  HELL_GENERATE_POINTER_WRAPPER(4, const);
  HELL_GENERATE_POINTER_WRAPPER(5, const);
  HELL_GENERATE_POINTER_WRAPPER(6, const);
  HELL_GENERATE_POINTER_WRAPPER(7, const);
  HELL_GENERATE_POINTER_WRAPPER(8, const);
  HELL_GENERATE_POINTER_WRAPPER(9, const);
  HELL_GENERATE_POINTER_WRAPPER(10, const);
  HELL_GENERATE_POINTER_WRAPPER(11, const);
  HELL_GENERATE_POINTER_WRAPPER(12, const);
  HELL_GENERATE_POINTER_WRAPPER(13, const);
  HELL_GENERATE_POINTER_WRAPPER(14, const);
  HELL_GENERATE_POINTER_WRAPPER(15, const);
  HELL_GENERATE_POINTER_WRAPPER(16, const);
  HELL_GENERATE_POINTER_WRAPPER(17, const);
  HELL_GENERATE_POINTER_WRAPPER(18, const);
  HELL_GENERATE_POINTER_WRAPPER(19, const);
  HELL_GENERATE_POINTER_WRAPPER(20, const);

  HELL_GENERATE_POINTER_WRAPPER(1, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(2, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(3, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(4, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(5, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(6, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(7, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(8, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(9, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(10, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(11, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(12, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(13, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(14, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(15, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(16, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(17, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(18, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(19, const volatile);
  HELL_GENERATE_POINTER_WRAPPER(20, const volatile);

  HELL_GENERATE_POINTER_WRAPPER(1, volatile);
  HELL_GENERATE_POINTER_WRAPPER(2, volatile);
  HELL_GENERATE_POINTER_WRAPPER(3, volatile);
  HELL_GENERATE_POINTER_WRAPPER(4, volatile);
  HELL_GENERATE_POINTER_WRAPPER(5, volatile);
  HELL_GENERATE_POINTER_WRAPPER(6, volatile);
  HELL_GENERATE_POINTER_WRAPPER(7, volatile);
  HELL_GENERATE_POINTER_WRAPPER(8, volatile);
  HELL_GENERATE_POINTER_WRAPPER(9, volatile);
  HELL_GENERATE_POINTER_WRAPPER(10, volatile);
  HELL_GENERATE_POINTER_WRAPPER(11, volatile);
  HELL_GENERATE_POINTER_WRAPPER(12, volatile);
  HELL_GENERATE_POINTER_WRAPPER(13, volatile);
  HELL_GENERATE_POINTER_WRAPPER(14, volatile);
  HELL_GENERATE_POINTER_WRAPPER(15, volatile);
  HELL_GENERATE_POINTER_WRAPPER(16, volatile);
  HELL_GENERATE_POINTER_WRAPPER(17, volatile);
  HELL_GENERATE_POINTER_WRAPPER(18, volatile);
  HELL_GENERATE_POINTER_WRAPPER(19, volatile);
  HELL_GENERATE_POINTER_WRAPPER(20, volatile);
}

#endif // UTILS_WRAP_METHOD_HH_
