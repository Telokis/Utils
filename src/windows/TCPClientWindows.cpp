#include "windows/TCPClientWindows.hh"

Utils::Windows::TCPClientWindows::TCPClientWindows()
  : _socket(INVALID_SOCKET)
{
  WSADATA		wsaData;

  if ((WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)
	  throw Exceptions::TCPClientRuntimeException("Unable to startup properly");
}

Utils::Windows::TCPClientWindows::~TCPClientWindows()
{
  if (_socket != INVALID_SOCKET)
    closesocket(this->_socket);
  WSACleanup();
}

size_t		Utils::Windows::TCPClientWindows::write(const void *buf, size_t len) const
{
  int       res;

  if (_socket == INVALID_SOCKET)
	  throw Exceptions::TCPClientLogicException("Can't send because socket is uninitialized");
  res = ::send(_socket, static_cast<const char *>(buf), len, 0);
  return (res == SOCKET_ERROR ? -1 : res);
}

bool	    Utils::Windows::TCPClientWindows::connect(const std::string &ip, unsigned short int port)
{
  sockaddr_in		sin;

  if ((this->_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0)) == INVALID_SOCKET)
    return (false);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = inet_addr(ip.c_str());
  if (bind(this->_socket, (const struct sockaddr *)&sin, sizeof(sin)) == SOCKET_ERROR)
    return (false);
  if ((WSAConnect(_socket, (const struct sockaddr *)&sin, sizeof(sin), NULL, NULL, NULL, NULL)) == SOCKET_ERROR)
    return (false);
  return (true);
}

size_t		    Utils::Windows::TCPClientWindows::read(void *buf, size_t len)
{
  int			flags = 0;

  if (_socket == INVALID_SOCKET)
	  throw Exceptions::TCPClientLogicException("Can't receive because socket is uninitialized");
  int n = WSARecvEx(_socket, static_cast<char *>(buf), UTILS_SOCKET_CLIENT_BUFFER_SIZE - 1, &flags);
  return (n == SOCKET_ERROR ? -1 : n);
}

void			Utils::Windows::TCPClientWindows::initializeFromAccept(SOCKET &sock)
{
  if (sock == INVALID_SOCKET)
	  throw Exceptions::TCPClientRuntimeException("TCP is invalid");
  if (_socket != INVALID_SOCKET)
	  throw Exceptions::TCPClientRuntimeException("Already initialized");
  _socket = sock;
}