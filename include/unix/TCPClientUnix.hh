#ifndef TCPCLIENTUNIX_HH_
#define TCPCLIENTUNIX_HH_

#include <string>
#include "Exceptions.hh"
#include "interfaces/ISocketServer.hh"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE	(4096)

namespace		Utils
{
	namespace	Unix
	{
		class			TCPClientUnix : public ISocketClient
		{
			friend class	TCPServerUnix;
		public:
			TCPClientUnix();
			~TCPClientUnix();

		private:
			TCPClientUnix(const TCPClientUnix &);
			TCPClientUnix	&operator=(const TCPClientUnix &);

		public:
			virtual size_t	write(const void *buf, size_t len) const;
			virtual size_t	read(void *buf, size_t len);
			virtual bool	connect(const std::string &ip, unsigned short int port);

		private:
			void		initializeFromAccept(int sock);

		private:
			int			_socket;
		}; // TCPClientUnix
	} // Unix
} // Utils

#endif // TCPCLIENTUNIX_HH_
