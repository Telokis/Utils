/**
*	@file   ProducterFile.hh
*
*	@brief  Defines an stream handler based on a file.
*/

#ifndef PRODUCTERFILE_HH_
#define PRODUCTERFILE_HH_

#include "interfaces/IProducterStream.hh"
#include <fstream>

namespace	Utils
{
  /**
  *   @class  ProducterFile  ProducterFile.hh <ProducterFile.hh>
  *
  *   @brief  Defines a stream handler reading from a file.
  *
  *   @see        @ref IProducterStream describes a ProducterStream
  *   @see        @ref ProducterString reads from a std::string
  *   @see        @ref ProducterCin reads from std::cin
  *   @see        #UTILS_PRODUCTER_STREAM_BUFFER_SIZE is an helper tool
  */
  class			ProducterFile : public IProducterStream
  {
  public:
    /**
    *   @brief      Constructor taking the file's path
    *
    *   @exception  Utils::Exceptions::ProducterRuntimeException  if the file can't be opened.
    */
    ProducterFile(const std::string &filename);
    ~ProducterFile();                                 /**< Destructor */

  public:
    /**
    *   @brief        Function used to get the next available chunk.
    *
    *   @param[out]   ret   A boolean which will be set to false if an error occurred or
    *                       if there were nothing left to read.
    *
    *   @return       The chunk to process. Its size is up to #UTILS_PRODUCTER_STREAM_BUFFER_SIZE
    *                 characters
    */
    std::string		nextString(bool &ret);

    /**
    *   @brief        Function used to test whether the file has been fully read or not.
    *
    *   @return       True if the end of the file has been reached. False otherwise.
    */
    bool		isEOF() const;

  private:
    std::ifstream	_stream;                          /**< std::ifstream holding the file */
  };

}

#endif // PRODUCTERFILE_HH_
