/**
*   @file   add_reference.hh
*   @brief  Describes Utils::add_reference.
*/

#ifndef UTILS_ADD_REFERENCE_HH_
#define UTILS_ADD_REFERENCE_HH_

#include <small_object/instance_of.hh>

namespace Utils
{
  /**
  *   @struct   add_reference  add_reference.hh   <type_traits.hh>
  *   @brief    Adds a reference qualifier to @a T.
  *
  *   add_reference<T>::type == T &.
  *
  *   @tparam   T The type to add reference on.
  *
  *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
  *             show specialized versions.
  */
  template <typename T>
  struct  add_reference : instance_of<T &>
  {
  };

#if !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
  /**
  *   @brief    Partial specialization for T&.
  */
  template <typename T>
  struct  add_reference <T&> : instance_of<T&>
  {
  };

  /**
  *   @brief    Partial specialization for void. Error will occur.
  */
  template <>
  struct  add_reference <void>
  {
  };
#endif // !defined(_DOXYGEN) || defined(UTILS_SHOW_TEMPLATE_SPECIALIZATION)
}

#endif // UTILS_ADD_REFERENCE_HH_
