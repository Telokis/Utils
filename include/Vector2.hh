#ifndef VECTOR2_HH_
#define VECTOR2_HH_

#include <iostream>

namespace	    Utils
{

  template <typename T>
  class		    Vector2
  {
  public:
    Vector2();
    Vector2(T _x, T _y);
    Vector2(T a);
    virtual	    ~Vector2() {}
    Vector2(const Vector2 &ref);
    Vector2	    &operator=(const Vector2 &ref);

  public:
    // Add
    Vector2	    &operator+=(const Vector2 &ref);
    Vector2	    &operator+=(const T &n);
    Vector2	    operator+(const Vector2<T> &b) const;
    Vector2	    operator+(const T &b) const;

    // Sub
    Vector2	    &operator-=(const Vector2 &ref);
    Vector2	    &operator-=(const T &n);
    Vector2	    operator-(const Vector2<T> &b) const;
    Vector2	    operator-(const T &b) const;

    // Mult
    Vector2	    &operator*=(const Vector2 &ref);
    Vector2	    &operator*=(const T &n);
    Vector2	    operator*(const Vector2<T> &b) const;
    Vector2	    operator*(const T &b) const;

    // Div
    Vector2	    &operator/=(const Vector2 &ref);
    Vector2	    &operator/=(const T &n);
    Vector2	    operator/(const Vector2<T> &b) const;
    Vector2	    operator/(const T &b) const;

    // Compare
    bool	    operator==(const Vector2 &ref) const;
    bool	    operator!=(const Vector2 &ref) const;

  public:
    void	    normalize();
    void	    serialize(std::ostream &os) const;

  public:
    static Vector2	normalize(const Vector2 &v);

  public:
    T	        x;
    T	        y;
  }; // Vector2

  template <typename T>
  std::ostream	&operator<<(std::ostream &str, const Vector2<T> &v)
  {
    v.serialize(str);
    return (str);
  }

  typedef	Vector2<float>	            vec2;
  typedef	Vector2<float>	            fvec2;
  typedef	Vector2<int>	            ivec2;
  typedef	Vector2<double>	            dvec2;
  typedef	Vector2<char>	            cvec2;
  typedef	Vector2<unsigned int>		uivec2;
  typedef	Vector2<unsigned char>		ucvec2;
} // Utils

#endif // VECTOR2_HH_
