#ifndef REF_HH_
#define REF_HH_

#include <small_object/instance_of.hh>

namespace Utils
{
  template <typename T>
  struct reference_wrapper : Utils::instance_of<T>
  {
    explicit reference_wrapper(T &obj)
      : _obj(&obj)
    {
    }
    operator	T&() const
    {
      return (*_obj);
    }
    T		&unwrap() const
    {
      return (*_obj);
    }

  private:
    T		*_obj;
  };

  template <typename T>
  inline reference_wrapper<T>	ref(T &t)
  {
    return (reference_wrapper<T>(t));
  }

  template <typename T>
  inline reference_wrapper<const T>	cref(const T &t)
  {
    return (reference_wrapper<const T>(t));
  }
}

#endif // REF_HH_
