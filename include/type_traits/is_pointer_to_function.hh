/**
 *   @file   is_pointer_to_function.hh
 *   @brief  Describes Utils::is_pointer_to_function.
 */

#ifndef UTILS_IS_POINTER_TO_FUNCTION_HH_
#define UTILS_IS_POINTER_TO_FUNCTION_HH_

#include <type_traits/is_function.hh>
#include <type_traits/is_pointer.hh>
#include <type_traits/remove_pointer.hh>
#include <small_object/select_type.hh>

namespace Utils
{
  /**
   *   @struct   is_pointer_to_function  is_pointer_to_function.hh   <type_traits.hh>
   *   @brief    Tells whether @a T is pointer_to_function or not.
   *
   *   If @a T is pointer_to_function, is_pointer_to_function<T>::value == true.
   *
   *   @tparam   T The type to check for pointer_to_functionness.
   *
   *   @note     Define UTILS_SHOW_TEMPLATE_SPECIALIZATION to
   *             show specialized versions.
   */
  template <typename T>
  struct  is_pointer_to_function
    : select_type<is_pointer<T>::value,
		  is_function<typename remove_pointer<T>::type>,
		  false_type>::type
  {
  };
}

#endif // UTILS_IS_POINTER_TO_FUNCTION_HH_
